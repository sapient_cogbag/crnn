/**
Copyright 2019 sapient_cogbag <sapient_cogbag@protonmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include <crnn/io/iomappings.hpp>

namespace crnn::io {
    usize extent_t::get_end() const {
        return start + delta;
    }
    
    const extent_t& BaseMapper::get_extent() const {
        return this->extent;
    }
    
    void BaseMapper::run_io() {
        while(io_is_runnable()) {
            auto this_cycle_start = std::chrono::high_resolution_clock::now();
            go();
            std::this_thread::sleep_until(this_cycle_start + minimum_io_delay);
        }
    }
    
    BaseMapper::BaseMapper(
        extent_t extent, 
        std::chrono::nanoseconds minimum_polling_delay
    ) : extent(extent), minimum_io_delay(minimum_polling_delay) {
        
    }
    
    void BaseMapper::go() {}
    
    void BaseMapper::enable_start_io() {
        keep_doing_io = true;
    }
    
    void BaseMapper::stop_io() {
        keep_doing_io = false;
    }
    
    bool BaseMapper::io_is_runnable() const {
        return keep_doing_io;
    }
    
    usize BaseMapper::get_extent_offset(usize neuron_idx) const {
        return neuron_idx - extent.start;
    }

    
    MAPPER_TYPE BaseInputMapper::get_io_type() const {
        return MAPPER_TYPE_V::INPUT_MAPPER;
    }
    
    MAPPER_TYPE BaseOutputMapper::get_io_type() const {
        return MAPPER_TYPE_V::OUTPUT_MAPPER;
    }

    OUTPUT_MAPPER_MODE BaseDischargeTriggerOutputMapper::get_output_mode() const {
        return OUTPUT_MAPPER_MODE_V::DISCHARGE_TRIGGER;
    }
    
    OUTPUT_MAPPER_MODE BaseChargeScanOutputMapper::get_output_mode() const {
        return OUTPUT_MAPPER_MODE_V::CHARGE_SCAN;
    }
    
    
    MAPPER_TYPE BaseDischargeAndScanOutputMapper::get_io_type() const {
        return MAPPER_TYPE_V::OUTPUT_MAPPER;
    }
    
    OUTPUT_MAPPER_MODE BaseDischargeAndScanOutputMapper::get_output_mode() const {
        return OUTPUT_MAPPER_MODE_V::SCAN_AND_TRIGGER;
    }
    
    
    MAPPER_TYPE BaseInputAndDischargeTriggerIOMapper::get_io_type() const {
        return MAPPER_TYPE_V::IO_MAPPER;
    }
    
    OUTPUT_MAPPER_MODE BaseInputAndDischargeTriggerIOMapper::get_output_mode() const {
        return OUTPUT_MAPPER_MODE_V::DISCHARGE_TRIGGER;
    }
    
    
    MAPPER_TYPE BaseInputAndChargeScanIOMapper::get_io_type() const {
        return MAPPER_TYPE_V::IO_MAPPER;
    }
    
    OUTPUT_MAPPER_MODE BaseInputAndChargeScanIOMapper::get_output_mode() const {
        return OUTPUT_MAPPER_MODE_V::CHARGE_SCAN;
    }

    
    MAPPER_TYPE BaseIOMapper::get_io_type() const {
        return MAPPER_TYPE_V::IO_MAPPER;
    }
    
    OUTPUT_MAPPER_MODE BaseIOMapper::get_output_mode() const {
        return OUTPUT_MAPPER_MODE_V::SCAN_AND_TRIGGER;
    }
}

