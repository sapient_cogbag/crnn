/**
Copyright 2019 sapient_cogbag <sapient_cogbag@protonmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <vector>
#include <cstdint>
#include <type_traits>
#include <experimental/memory>


/**
 * Library namespace.
 * Also has some convenient typedefs for integers and floats (stealing them from Rust ^.^)
 */
namespace crnn {
    template <typename T>
    using observer_ptr = std::experimental::observer_ptr<T>;
    
    /**
     * Some typedefs to make code neater.
     */
    namespace microtypedefs {
        using u8 = std::uint8_t;
        using u16 = std::uint16_t;
        using u32 = std::uint32_t;
        using u64 = std::uint64_t;
        using usize = std::size_t;
        
        using i8 = std::int8_t;
        using i16 = std::int16_t;
        using i32 = std::int32_t;
        using i64 = std::int64_t;
        
        using f32 = float;
        using f64 = double;
        using fmax = long double;
        
        using u8c = const u8;
        using u16c = const u16;
        using u32c = const u32;
        using u64c = const u64;
        using usizec = const usize;
        
        using i8c = const i8;
        using i16c = const i16;
        using i32c = const i32;
        using i64c = const i64;
        
        using f32c = const f32;
        using f64c = const f64;
        using fmaxc = const fmax;
        
    }
    
    using namespace microtypedefs;
}

/**
 * Namespace for stuff related to on-disk persistence. 
 * We're going for simple dump-to-stream-no-error-checking here.
 * 
 */
namespace crnn::persistence {
    using namespace microtypedefs;
}

/**
 * Namespace for general utilities.
 */
namespace crnn::util {
    using namespace microtypedefs;
}

/**
 * Namespace for simple threading utilities.
 */
namespace crnn::threading {
    using namespace microtypedefs;
    class SimpleThreadPool;
    
}

/**
 * Associatively-indexed memory.
 * 
 * Provides for turning associations between the indexes of values in a dataset into datastructures that store
 * a given value in any of the sets at the index of any of the values it is associated with in their index systems.
 */
namespace crnn::aimem {  
    
    using index_t = usize;
    
    template <index_t ReadFromISys, index_t WriteToISys = ReadFromISys>
    struct index_system_pair_t;
    
    /**
     * enum for whether or not the index system uses a 1-to-1 index<->value map, a 1-to-constant index<->value map,
     * or a <1-to-varying> index<->value map
     */
    enum class IDX_SYSTEM_TYPE {
        UNIQUE,
        FIXED_SIZE,
        DYNAMIC_SIZE
    };
}


/**
 * For translating IO things to data for neural network stuff.
 */
namespace crnn::io {
    struct extent_t;
    
    class BaseMapper;
    
    // Classes used to select the mode(s) of processing.
    class BaseInputMapper;
    class BaseChargeScanOutputMapper;
    class BaseDischargeTriggerOutputMapper;
    
    // all output mappers use this base class.
    class BaseOutputMapper;
    
    // Convenient combined type classes.
    class BaseDischargeAndScanOutputMapper;
    class BaseInputAndDischargeTriggerIOMapper;
    class BaseInputAndChargeScanIOMapper;
    class BaseIOMapper;
    
    /**
     * The type of the mapper object.
     */
    struct MAPPER_TYPE {
        bool input : 1 = false;
        bool output : 1 = false;
    };
    
    inline constexpr MAPPER_TYPE operator|(const MAPPER_TYPE a, const MAPPER_TYPE b){
        return MAPPER_TYPE{a.input || b.input, a.output || b.output};
    }
    
    inline constexpr MAPPER_TYPE operator&(const MAPPER_TYPE a, const MAPPER_TYPE b) {
        return MAPPER_TYPE{a.input && b.input, a.output && b.output};
    }
    
    // Gets if there is any overlap.
    inline constexpr bool operator&&(const MAPPER_TYPE& a, const MAPPER_TYPE& b) {
        const auto combined = a & b;
        return combined.input || combined.output;
    }
    
    namespace MAPPER_TYPE_V {
        constexpr MAPPER_TYPE INPUT_MAPPER = {true, false};
        constexpr MAPPER_TYPE OUTPUT_MAPPER = {false, true};
        constexpr MAPPER_TYPE IO_MAPPER = INPUT_MAPPER | OUTPUT_MAPPER;
    }
    
    /**
     * The mode of an output mapper object - continuous scan of neuron charges or triggered on discharge.
     */
    struct OUTPUT_MAPPER_MODE {
        bool charge_scan : 1 = false;
        bool discharge_trigger : 1 = false;
    };
    
    inline constexpr OUTPUT_MAPPER_MODE operator|(const OUTPUT_MAPPER_MODE a, const OUTPUT_MAPPER_MODE b) {
        return OUTPUT_MAPPER_MODE{a.charge_scan || b.charge_scan, a.discharge_trigger || b.discharge_trigger};
    }
    
    inline constexpr OUTPUT_MAPPER_MODE operator&(const OUTPUT_MAPPER_MODE a, const OUTPUT_MAPPER_MODE b) {
        return OUTPUT_MAPPER_MODE{a.charge_scan && b.charge_scan, a.discharge_trigger && b.discharge_trigger};
    }
    
    // Gets whether there is any overlap.
    inline constexpr bool operator&&(const OUTPUT_MAPPER_MODE& a, const OUTPUT_MAPPER_MODE& b) {
        const auto combined = a & b;
        return combined.charge_scan || combined.discharge_trigger;
    }
    
    namespace OUTPUT_MAPPER_MODE_V {
        constexpr OUTPUT_MAPPER_MODE CHARGE_SCAN = OUTPUT_MAPPER_MODE{true, false};
        constexpr OUTPUT_MAPPER_MODE DISCHARGE_TRIGGER = OUTPUT_MAPPER_MODE{false, true};
        constexpr OUTPUT_MAPPER_MODE SCAN_AND_TRIGGER = CHARGE_SCAN | DISCHARGE_TRIGGER;
    }
    
}

/**
 * For neural network classes.
 */
namespace crnn::nn {
    struct NeuronChargeData;
    struct NeuronChargeDeltaData;
    
    struct NeuronActivationData;
    struct NeuronActivationUpdateData;
    
    struct NeuronConnectionData;
    struct NeuronConnectionDeltaData;
    
    struct FullNeuronData;
    struct FullNeuronDeltaData;
    
    struct NeuralNetworkData;
    struct NeuralNetworkProcessor;
}
