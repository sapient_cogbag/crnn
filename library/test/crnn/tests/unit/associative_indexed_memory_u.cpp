/**
Copyright 2019 sapient_cogbag <sapient_cogbag@protonmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include <catch2/catch.hpp>
#include <crnn/associative_indexed_memory.hpp>
#include <iostream>

class test_man_ns_t : public crnn::aimem::idx_system_data_manager_t<3, test_man_ns_t> {
    
};

TEST_CASE("Association Sorted Map Testing", "[crnn][crnn::aimem]") {
    
    using namespace crnn::microtypedefs;
    using as = test_man_ns_t::association_t;
    std::vector <as> assocs_data_raw;
    assocs_data_raw.push_back(as({0, 1, 0}));
    assocs_data_raw.push_back(as({0, 2, 1}));
    assocs_data_raw.push_back(as({1, 0, 1}));
    assocs_data_raw.push_back(as({2, 1, 1}));
    assocs_data_raw.push_back(as({0, 0, 3}));
    
    
    auto assocs = test_man_ns_t::association_data_accessor_t<
        crnn::aimem::idx_association_storage::idx_association_storage_order_t<0, 1>
    >(test_man_ns_t::association_data_t(std::vector<as>(
        assocs_data_raw.begin(), 
        assocs_data_raw.end()
    ))); 
    
    
    SECTION("Sorting -> into an index map") {  
        
        auto idx_index_map = assocs.to_index_data();
        
        // Get the indices in system 1 associated with index 0 in system 0
        decltype(idx_index_map)::raw_data_iterator_t start_iter, end_iter;
        
        std::tie(start_iter, end_iter) = idx_index_map.get_for_index(0);
        
        std::vector<usize> idxs_0_1(start_iter, end_iter);
        REQUIRE(idxs_0_1[0] == 0);
        REQUIRE(idxs_0_1[1] == 1);
        REQUIRE(idxs_0_1[2] == 2);
        REQUIRE(idxs_0_1.size() == 3);
        
        std::tie(start_iter, end_iter) = idx_index_map.get_for_index(1);
        std::vector<usize> idxs_1_1(start_iter, end_iter);
        REQUIRE(idxs_1_1[0] == 0);
        REQUIRE(idxs_1_1.size() == 1);
        
        std::tie(start_iter, end_iter) = idx_index_map.get_for_index(2);
        std::vector<usize> idxs_2_1(start_iter, end_iter);
        REQUIRE(idxs_2_1[0] == 1);
        REQUIRE(idxs_2_1.size() == 1);
    };
    
    SECTION("Sorting -> into another association data storage accessor") {
        // Associations ordered from 1 to 2
        auto association_acs_1_2 = assocs.clone_as<crnn::aimem::idx_association_storage_order_t<1, 2>>();
        auto idx_index_map = association_acs_1_2.to_index_data();
        decltype(idx_index_map)::raw_data_iterator_t start_iter, end_iter;
        
        // Index system 1, index 0
        std::tie(start_iter, end_iter) = idx_index_map.get_for_index(0);
        REQUIRE(std::vector<usize>(start_iter, end_iter) == std::vector<usize>{1, 3});
        std::tie(start_iter, end_iter) = idx_index_map.get_for_index(1);
        REQUIRE(std::vector<usize>(start_iter, end_iter) == std::vector<usize>{0, 1});
        std::tie(start_iter, end_iter) = idx_index_map.get_for_index(2);
        REQUIRE(std::vector<usize>(start_iter, end_iter) == std::vector<usize>{1});
        
    };
    
};

TEST_CASE("Indexed Data Map Testing", "[crnn][crnn::aimem]") {
    
};
