/**
Copyright 2019 sapient_cogbag <sapient_cogbag@protonmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <crnn/predecl.hpp>
#include <future>
#include <thread>
#include <deque>
#include <vector>
#include <condition_variable>
#include <atomic>
#include <variant>
#include <list>

namespace crnn::threading {
    using namespace microtypedefs;
    
    /**
     * Simple thread-safe queue. Multi-in and multi-out feeding. Uses std::deque
     */
    template <typename QueueItem>
    class ThreadSafeMultiIOQueue {
        std::deque <QueueItem> queue = {};
        std::mutex queue_lock;
        // Triggered once every time an item is added to or removed from the queue.
        // Only uses notify_all();
        std::condition_variable queue_element_modified;
        std::atomic <usize> queue_element_count = 0;
        
    public:
        
        // Wait for more than the provided count of items to be available,
        void wait_for_at_least(usize count) {
            if (queue_element_count >= count) {
                return;
            }
            
            std::unique_lock lock(queue_lock);
            queue_element_modified.wait(lock, [&]() -> bool {
                return queue_element_count >= count;
            });
            
        }
        /**
         * Wait for an item to be available on the queue.
         */
        void wait_for_item() {
            wait_for_at_least(1);
        }
        
        /**
         * Get an item from the queue, waiting for such.
         */
        [[nodiscard]] QueueItem get_item() {
            wait_for_item();
            
            // Lock afterward so we don't deadlock with wait_for_item
            QueueItem a;
            std::lock_guard lock(queue_lock);
            
            // We lose one item so decrement it
            queue_element_count--;
            a = queue.front();
            queue.pop_front();
            
            // Notify that the queue has changed.
            queue_element_modified.notify_all();
            
            return a;
        }
        
        /**
         * Post an item onto the queue.
         */
        void post_item(QueueItem a) {
            std::lock_guard lock(queue_lock);
            queue_element_count++;
            queue.push_back(a);
            queue_element_modified.notify_all();
        }
        
        // Wait for the queue to be empty
        void wait_for_empty() {
            if (queue_element_count == 0) {
                return;
            }
            
            std::unique_lock lock (queue_lock);
            queue_element_modified.wait(queue_lock, [&]() -> bool {
                return queue_element_count == 0;
            });
        }
    };
    
    /**
     * Simple threadpool for thread scheduling.
     */
    class SimpleThreadPool {
    public:
        enum class THREAD_SIGNAL {
            DIE
        };
        
        enum class THREAD_STATE {
            UNINITIALIZED,
            RUNNING,
            DEAD
        };
        
        using runnable_t = std::function <void()>;
        using queueable_task_t = std::variant<
            runnable_t,
            THREAD_SIGNAL
        >;
        
        // stdlib has no way to inspect whether a thread is finished or not, so store the state ourselves.
        using pool_thread_t = std::pair<
            std::unique_ptr <std::atomic <THREAD_STATE>>,
            std::unique_ptr <std::thread>
        >;
        
    private:
        ThreadSafeMultiIOQueue <queueable_task_t> tasks;
        
        /**
         * Number of long-term live threads.
         * 
         * This can be negative in the case that there are pending kill signals such that any new thread
         * will immediately die. This basically counts how many threads will wait for tasks "eventually".
         */
        std::atomic <i64> to_live_thread_count = 0;
        
        /**
         * Number of currently alive threads.
         */
        std::atomic <usize> currently_alive_thread_count = 0;
        
        /**
         * This is the mutex for managing the list of threads and 
         * associated modification condition variable.
         */
        std::mutex pool_mtx;
        
        /**
         * Triggered when:
         * * A thread is started
         * * A thread is scheduled to start.
         * * A thread actually dies (not when a thread is *scheduled* to die).
         * * A thread is scheduled to die.
         */
        std::condition_variable pool_updated;
        
        /**
         * Use a list so we can easily remove dead threads.
         * The 
         */
        std::list <pool_thread_t> pool = {};
        
        /**
         * The function to get tasks.
         */
        void task_executor();
        
        
        /**
         * The function actually executed on each thread.
         */
        void thread_executor();
        
        
    public:
        /**
         * Default constructor uses std::thread::hardware_concurrency().
         */
        SimpleThreadPool();
        SimpleThreadPool(usize thread_count);
        
        void create_threads(usize thread_count);
        
        /**
         * Tell threads to die. 
         * 
         * If you specify more than the total current live threads in signals, all will be killed,
         * and you end up with leftover kill signals. Attempting to create new threads will result in them
         * being killed (and running any new tasks before the given kill signals) until all the kill signals
         * are cleared from the queue.
         */
        void schedule_kill_threads(usize killsignal_count);
        
        /**
         * Schedule the task on the queue. Note that the arguments provided are copied by value, so if you
         * want to capture stuff by reference then you should encapsulate that in a lambda.
         * 
         * Returns a future object for waiting for the result of the task.
         */
        template <typename Callable, typename... Args>
        [[nodiscard]] auto schedule_task(Callable&& sched, Args&&... args) {
            using ReturnType = decltype(sched(std::forward<Args>(args)...));
            // All this does is put the function into a task so we can get a future rather than run directly.
            std::packaged_task task(sched);
            std::future <ReturnType> fut = task.get_future();
            // Add a task to run the task to the queue.
            tasks.post_item([&task, args...](){
                std::packaged_task actual_task = std::move(task); // Pull the task into this stack.
                actual_task(args...);
            });
            
            return fut;
        }
        
        /**
         * Schedule for all live threads not already scheduled to die to be killed.
         */
        void schedule_kill_all_threads();
        
        /**
         * Wait for all threads to die (i.e. for currently_alive_thread_count to be 0).
         */
        void wait_for_all_threads_to_die();
        
        /**
         * Remove all dead threads from the thread pool.
         */
        void remove_dead_threads();
        
        /**
         * Wait for all threads currently scheduled to die, to die.
         */
        void wait_for_scheduled_kills();
        
        /**
         * Destructor schedules all threads to die and waits for that.
         */
        virtual ~SimpleThreadPool();
        
    };
    
}
