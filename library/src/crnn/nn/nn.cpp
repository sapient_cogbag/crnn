/**
Copyright 2019 sapient_cogbag <sapient_cogbag@protonmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <crnn/nn/nn.hpp>
#include <variant>
#include <stack>

namespace crnn::nn {
    void NeuronChargeData::combine(const NeuronChargeDeltaData& delta) {
        this->activation_threshold += delta.activation_threshold_increment;
        this->charge += delta.charge_increment;
        this->decay_time_factor_constant *= delta.decay_rate_multiplier;
    }
    
    NeuronChargeDeltaData combine(const NeuronChargeDeltaData& a, const NeuronChargeDeltaData& b) {
        return NeuronChargeDeltaData {
            a.charge_increment + b.charge_increment,
            a.activation_threshold_increment + b.activation_threshold_increment,
            a.decay_rate_multiplier * b.decay_rate_multiplier
        };
    }
    
    void NeuronActivationData::update(const NeuronActivationUpdateData& a) {
        if (a.last_activation_time >= this->last_activation_time) {
            this->last_activation_time = a.last_activation_time;
            this->charge_at_time = a.charge_at_time;
            this->threshold_at_time = a.threshold_at_time;
        }
    }
    
    NeuronActivationUpdateData combine(const NeuronActivationUpdateData& a, const NeuronActivationUpdateData& b) {
        return a.charge_at_time >= b.charge_at_time ? a : b;
    }
    
    
    FullNeuronDeltaData combine(const FullNeuronDeltaData& a, const FullNeuronDeltaData& b) {
        return {
            combine(a.charge_delta, b.charge_delta),
            combine(a.activation_update, b.activation_update)
        };
    }
    
    void FullNeuronData::combine(const FullNeuronDeltaData& a) {
        combine(a.charge_delta);
        combine(a.activation_update);
    }
    
    void FullNeuronData::combine(const NeuronChargeDeltaData& a) {
        charge_data.combine(a);
    }
    
    void FullNeuronData::combine(const NeuronActivationUpdateData& a) {
        activation_data.update(a);
    }
    
    void NeuronConnectionData::combine(const NeuronConnectionDeltaData& delta) {
        weight += delta.weight_delta;
        
        sensitization_coefficient += delta.sensitization_coefficient_delta;
        desensitization_threshold += delta.desensitization_threshold_delta;
        sensitization_renormalisation_constant *= delta.sensitization_renormalisation_constant_multiplier;
        
        resensitization_time_threshold += delta.resensitization_time_threshold_delta;
        last_activation_time = last_activation_time > delta.last_activation_time ? 
            last_activation_time : delta.last_activation_time;
        
    }
}

