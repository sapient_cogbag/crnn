/**
Copyright 2019 sapient_cogbag <sapient_cogbag@protonmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <crnn/predecl.hpp>
#include <crnn/util.hpp>
#include <atomic>

// Implements a triple buffering system for multithreading and IO purposes.

namespace crnn::util {
    
    /**
     * Holds a triple buffer object.
     */
    template <typename T>
    class triple_buffer {
    public:
        using buffer_t = std::vector <T>;
        using buffer_ptr_t = std::unique_ptr <buffer_t>;
    protected:
        // We have the complete buffer, writeable buffer, and "display" buffer.
        // We swap the complete and display buffer to update "display" data
        // We swap the write and complete buffer when the complete buffer is not being
        // copied and the write buffer is complete.
        
        std::array<buffer_t, 3> buffers;
        
        u8 write_buffer = 0;
        u8 completed_buffer = 1;
        u8 display_buffer = 2;
        
        std::atomic_flag completed_buffer_is_dirty = ATOMIC_FLAG_INIT;
        
        std::atomic_flag write_locked = ATOMIC_FLAG_INIT; // Set while the write buffer is being written to.
        std::atomic_flag completed_locked = ATOMIC_FLAG_INIT; // Set while complete is being copied from.
        std::atomic_flag display_locked = ATOMIC_FLAG_INIT; // Set while display is being copied to and/or processed.
        
        usizec buffer_size;
        
    public:
        // Initialise everything
        triple_buffer(usize length) : buffer_size(length) {
            for (buffer_t& b: buffers) {
                b.resize(length);
            }
        }
        
        usize size() const {
            return buffer_size;
        }
        
        /**
         * Acquire/lock the writing buffer for writing.
         * 
         * Returns if we actually managed to acquire it.
         */
        [[nodiscard]] bool start_writing() {
            return !write_locked.test_and_set(std::memory_order::acquire);
        }
        
        /**
         * Tell the buffer that writing to the write buffer has been completed. 
         * Unlocks the write buffer.
         * If the complete buffer is not locked in copying then it will be swapped with the write buffer.
         * 
         * Returns true if we actually swapped, false if we didn't.
         * 
         * Also marks the completed buffer as dirty if it has been swapped to.
         * 
         * If you want to force a swap into the complete buffer, then you can do so by doing a `while(!done_writing())`
         * which acts as a spinlock.
         */
        bool done_writing() {
            util::unconditionally_on_exit([this](){
                write_locked.clear(std::memory_order::release);
            });
            
            // If not already acquired, aquire and swap, then set the dirty flag.
            if (!completed_locked.test_and_set(std::memory_order::acquire)) {
                std::swap(write_buffer, completed_buffer);
                completed_buffer_is_dirty.test_and_set();
                completed_locked.clear(std::memory_order::release);
                return true;
            }
            return false;
        }
        
        /**
         * Try to acquire the display lock. If the completed buffer is not currently locked and is dirty, 
         * then we lock it and swap it into the display buffer then unlock it, so you always get the latest display 
         * buffer.
         * Returns if we actually acquired the display buffer.
         * 
         * If you want to force-acquire the display buffer, you can do `while (!start_displaying());`, which acts as
         * a simple spinlock. The cheap nature of swap operations means this is very unlikely to block for more than
         * a very small amount of time.
         */
        [[nodiscard]] bool start_displaying() {
            // Attempt to acquire the display lock.
            if (!display_locked.test_and_set(std::memory_order::acquire)) {
                // Then, attempt to acquire the complete buffer and swap it with the display buffer
                // to get the latest version, if the complete buffer has been written since the 
                // last time we swapped with it.
                if(completed_buffer_is_dirty.test_and_set()) {
                    if (!completed_locked.test_and_set(std::memory_order::acquire)) {
                        std::swap(completed_buffer, display_buffer);
                        completed_buffer_is_dirty.clear(); // The completed buffer is no longer dirty.
                        completed_locked.clear(std::memory_order::release);
                    } // No swap if the completed buffer is being used. This is fine.
                } else {
                    // We set the flag but we don't want it to be set.
                    completed_buffer_is_dirty.clear();
                }                
                return true; // successfully acquired.
            }
            return false; 
        }
        
        /**
         * Unacquire the display buffer lock.
         */
        void done_displaying() {
            display_locked.clear(std::memory_order::release);
        }
        
        /**
         * Get a reference to the write buffer.
         */
        buffer_proxy<T> get_write_buffer() {
            return buffers[write_buffer];
        }
        
        /**
         * Get a reference to the display buffer.
         * 
         * Note that it can technically be written to (the writes will be preserved until the display buffer is
         * swapped out). This means that if you make multiple passes over the display buffer, then you can preserve
         * changes and avoid duplicating work if you have some kind of steady state that only gets modified on changes.
         */
        buffer_proxy<T> get_display_buffer() {
            return buffers[display_buffer];
        }
    };
}
