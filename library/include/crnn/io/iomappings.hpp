/**
Copyright 2019 sapient_cogbag <sapient_cogbag@protonmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <crnn/predecl.hpp>
#include <variant>
#include <stack>
#include <condition_variable>
#include <future>

namespace crnn::io {
    
    /**
     * Holds data covering a certain number of neurons.
     */
    struct extent_t {
        // Start neuron
        usize start;
        
        // Length of contiguous neurons.
        usize delta;
        usize get_end() const;
    };
    
    /**
     * A very basic mapper for IO <=> neuron mapping.
     * 
     * Is the base class for actual maps.
     * 
     * The io is executed in it's own thread, and other functions are called from a different thread. 
     * The go() virtual method is called repeatedly until the user requests i.o. to stop, so you can
     * either poll in this method or run a long thread. Polling is preferred because it also allows
     * the io delay system to work too.
     */
    class BaseMapper {
    protected: 
        extent_t extent;
        virtual void go();
        std::atomic<bool> keep_doing_io;
        /**
         * The minimum time between successive calls to the io processing method.
         */
        std::chrono::nanoseconds minimum_io_delay;
    public:
        virtual ~BaseMapper();
        const extent_t& get_extent() const;
        
        /**
         * @param extent the neurons to map to.
         * @param minimum_polling_delay The minimum time between successive calls to go()
         */
        BaseMapper(extent_t extent, std::chrono::nanoseconds minimum_polling_delay);
        
        /**
         * Mark i/o as "should continue running" atomically.
         */
        virtual void enable_start_io() final;
        
        /**
         * Tell the mapper it should stop processing IO.
         * 
         * This sets a variable atomically which means that the next go() will not be called
         * once the current one returns.
         */
        virtual void stop_io() final;
        
        /**
         * function that runs i/o. Will be called in a different thread to that which grabs updated
         * data from the io mapper or provides neuron data to the io mapper.
         */
        virtual void run_io() final;
        
        /**
         * Returns whether or not the io should keep running or will run if you start it.
         */
        virtual bool io_is_runnable() const final;
        
        /**
         * Get whether or not the object is a an input, output, or both, mapper.
         */
        virtual MAPPER_TYPE get_io_type() const = 0;
        
        /**
         * Get the offset into the extent (distance from the start neuron of the extent) 
         * of the given neuron index. This is UB for neurons outside the range of the
         * extent (excluding if the neuron index is one more than the index of the end of
         * the extent) for performance reasons.
         */
        virtual usize get_extent_offset(usize neuron_idx) const final;
    };
    
    /**
     * A base class for input mappers.
     */
    class BaseInputMapper : public BaseMapper {
    public:
        virtual MAPPER_TYPE get_io_type() const override;
        
        /**
         * Get the rate of charge increase for the given neuron index - 
         * guarunteed to be within the extents provided by get_extent().
         * 
         * This will be called in a different thread to the io thread.
         * 
         * @param neuron_idx index of neuron to get charge increase rate in units per nanosecond 
         * (needs shifting to extent)
         * @param t Simulation time in nanoseconds. Note that a simulation may renormalise this back to
         * zero at any point.
         * @param dt the change in time for the current processing cycle.
         * 
         */
        virtual f64 get_charge_increase_rate(
            usize neuron_idx, 
            const std::chrono::nanoseconds& t, 
            const std::chrono::nanoseconds& dt
        ) const = 0;
        
        
    };
    
    /**
     * Base class for output IO mappers.
     */
    class BaseOutputMapper : public BaseMapper {
    public:
        virtual MAPPER_TYPE get_io_type() const override;
        
        virtual OUTPUT_MAPPER_MODE get_output_mode() const = 0;
    };
    
    /**
     * Base class for output mappers based on the current value of a neuron's charge.
     */
    class BaseChargeScanOutputMapper : public BaseOutputMapper {
    public:
        virtual OUTPUT_MAPPER_MODE get_output_mode() const override;
        
        /**
         * Tell the output mapper to update the charge at the given neuron index 
         * (needs shifting to extent, but is guarunteed to be within the extent)
         * 
         * This will be called from a thread other than the io thread.
         * 
         * @param neuron_idx The index of the neuron to update the charge for. (not shifted to the extent) (guarunteed
         *  to be within the extent.
         * @param new_charge_value The updated charge of the neuron.
         * @param t The simulation time, can be zeroed at any time in the simulation.
         * @param dt The dt of the numeric approximation cycle.
         */
        virtual void update_with_charge(
            usize neuron_idx,
            const f64& new_charge_value,
            const std::chrono::nanoseconds& t,
            const std::chrono::nanoseconds& dt
        ) = 0;
    };
    
    /**
     * Base class for output mappers based on the discharging of neurons.
     */
    class BaseDischargeTriggerOutputMapper: public BaseOutputMapper {
    public:
        virtual OUTPUT_MAPPER_MODE get_output_mode() const override;
        
        /**
         * Tell the output mapper that a neuron has been triggered to discharge.
         * 
         * This will be called from a thread other than the io thread.
         * 
         * @param neuron_idx The index of the neuron that was triggered. Guarunteed to be in the
         *  extent for this iomapper.
         * @param amount_discharged The amount of charge the neuron had before discharging itself.
         * @param t The time in the simulation (can become zero at arbitrary time).
         * @param dt The time interval used in the simulation when the neuron discharged.
         */
        virtual void on_neuron_discharge(
            usize neuron_idx,
            const f64& amount_discharged,
            const std::chrono::nanoseconds& t,
            const std::chrono::nanoseconds& dt
        ) = 0;
    };
    
    //BEGIN Utility combined classes

    /**
     * Output mapper that both scans neuron values and acts on discharge triggers.
     * 
     * Classes inheriting from this cannot change the IO mode or output mode.
     */
    class BaseDischargeAndScanOutputMapper : public BaseDischargeTriggerOutputMapper, BaseChargeScanOutputMapper {
    public:
        virtual MAPPER_TYPE get_io_type() const override final;
        virtual OUTPUT_MAPPER_MODE get_output_mode() const override final;
    };
    
    /**
     * IO Mapper that does input mapping as well as activating on a discharge trigger for a neuron.
     * 
     * Classes inheriting from this cannot change the IO mode or output mode.
     */
    class BaseInputAndDischargeTriggerIOMapper : public BaseInputMapper, BaseDischargeTriggerOutputMapper {
    public:
        virtual MAPPER_TYPE get_io_type() const override final;
        virtual OUTPUT_MAPPER_MODE get_output_mode() const override final;
    };
    
    /**
     * IO Mapper that does input mapping as well as scanning neuron charges.
     * 
     * Classes inheriting from this cannot change the IO mode or output mode.
     */
    class BaseInputAndChargeScanIOMapper : public BaseInputMapper, BaseChargeScanOutputMapper {
    public:
        virtual MAPPER_TYPE get_io_type() const override final;
        virtual OUTPUT_MAPPER_MODE get_output_mode() const override final;
    };
    
    /**
     * IO Mapper that does all three forms of IO.
     * 
     * Classes inheriting from this cannot change the IO mode or output mode.
     */
    class BaseIOMapper: public BaseInputMapper, BaseDischargeTriggerOutputMapper, BaseChargeScanOutputMapper {
    public:
        virtual MAPPER_TYPE get_io_type() const override final;
        virtual OUTPUT_MAPPER_MODE get_output_mode() const override final;
    };
    //END
    
    
}
