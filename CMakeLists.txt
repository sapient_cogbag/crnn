cmake_minimum_required(VERSION 3.9.4)
project(crnn)
find_package(Catch2)

set(CMAKE_CXX_STANDARD 20)


set(CRNN_SOURCES
    library/src/crnn/predecl.cpp
    library/src/crnn/persistence.cpp
    library/src/crnn/util.cpp
    library/src/crnn/threading.cpp
    library/src/crnn/associative_indexed_memory.cpp
    library/src/crnn/io/iomappings.cpp
    library/src/crnn/io/triple_buffer.cpp
    library/src/crnn/nn/nn.cpp
)

# Make it shared
add_library(crnn
    SHARED 
    ""
)

target_sources(crnn PRIVATE
    ${CRNN_SOURCES}
)


# Also provide a static one
add_library(crnn_static 
    STATIC 
    ""
)

target_sources(crnn_static PRIVATE
    ${CRNN_SOURCES}
)
    
target_include_directories(crnn PUBLIC library/include)
target_include_directories(crnn_static PUBLIC library/include)

# These two mean that only when actually building the library (rather than building the headers but added to 
# another project) the compiler definition -DRCNN_LIB_BUILDING_LIBRARY will exist.
target_compile_definitions(crnn PRIVATE -DCRNN_LIB_BUILDING_LIBRARY)
target_compile_definitions(crnn_static PRIVATE -DCRNN_LIB_BUILDING_LIBRARY)

# These two definitions are present both in library builds and when linking with the library.
target_compile_definitions(crnn PUBLIC -DCRNN_LIBRARY_SHARED)
target_compile_definitions(crnn_static PUBLIC -DCRNN_LIBRARY_STATIC)

target_compile_options(crnn PRIVATE -Wall -Wextra -O4 -g3)
target_compile_options(crnn_static PRIVATE -Wall -Wextra -O4 -g3)

target_link_libraries(crnn pthread)
target_link_libraries(crnn_static pthread)

# LTO
set_target_properties(crnn_static crnn PROPERTIES INTERPROCEDURAL_OPTIMIZATION True)

add_executable(crnn_client basic_client/src/main.cpp)

target_link_libraries(crnn_client crnn_static)
target_include_directories(crnn_client PUBLIC basic_client/include)

# LTO
set_target_properties(crnn_client PROPERTIES INTERPROCEDURAL_OPTIMIZATION True)

if(Catch2_FOUND)
    enable_testing(true)
    include(CTest)
    include(Catch)
    include(ParseAndAddCatchTests)
    set(CRNN_TEST_BENCHMARK_MAIN
        library/test/crnn/test_main.cpp
    )
    set(CRNN_TEST_SOURCES
        library/test/crnn/tests/unit/persistence_u.cpp
        library/test/crnn/tests/unit/util_u.cpp
        library/test/crnn/tests/unit/associative_indexed_memory_u.cpp
        library/test/crnn/tests/unit/io/iomappings_u.cpp
        library/test/crnn/tests/unit/io/triple_buffer_u.cpp
        library/test/crnn/tests/unit/nn/nn_u.cpp
    )
    
    set(CRNN_TEST_LIBRARY_SOURCES
        library/test/crnn/src/crnn-test/crnn-test.cpp
    )
    
    add_library(crnn_test_library STATIC "")
    target_sources(crnn_test_library PRIVATE ${CRNN_TEST_LIBRARY_SOURCES})
    target_include_directories(crnn_test_library PUBLIC library/test/crnn/include/)
    target_link_libraries(crnn_test_library crnn)
    
    set(CRNN_BENCHMARK_SOURCES
    
    )
    
    
    add_executable(crnn_test "")
    add_executable(crnn_benchmark "")
    
    target_sources(crnn_test PRIVATE
        ${CRNN_TEST_BENCHMARK_MAIN}
        ${CRNN_TEST_SOURCES}
    )
    target_sources(crnn_benchmark PRIVATE
        ${CRNN_TEST_BENCHMARK_MAIN}
        ${CRNN_BENCHMARK_SOURCES}
    )
    
    target_include_directories(crnn_test PUBLIC library/include)
    target_include_directories(crnn_benchmark PUBLIC library/include)
    
    target_link_libraries(crnn_test crnn_test_library crnn_static Catch2::Catch2)
    target_link_libraries(crnn_benchmark crnn_test_library crnn_static Catch2::Catch2)
    
    target_compile_definitions(crnn_test PUBLIC CATCH_CONFIG_ENABLE_BENCHMARKING)
    target_compile_definitions(crnn_benchmark PUBLIC CATCH_CONFIG_ENABLE_BENCHMARKING)
    
    # LTO
    set_target_properties(crnn_test crnn_benchmark PROPERTIES INTERPROCEDURAL_OPTIMIZATION True)
    
    #catch_discover_tests(crnn_test)
    ParseAndAddCatchTests(crnn_test)
    ParseAndAddCatchTests(crnn_benchmark)
endif()


install(TARGETS crnn_client crnn crnn_static 
    RUNTIME DESTINATION bin 
    ARCHIVE DESTINATION lib 
    LIBRARY DESTINATION lib
)
