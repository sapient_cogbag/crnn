/**
Copyright 2019 sapient_cogbag <sapient_cogbag@protonmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <crnn/threading.hpp>
#include <crnn/util.hpp>

namespace crnn::threading {
    SimpleThreadPool::SimpleThreadPool() : SimpleThreadPool(std::thread::hardware_concurrency()){}
    
    SimpleThreadPool::SimpleThreadPool(usize thread_count) {
        this->create_threads(thread_count);
    }
    
    void SimpleThreadPool::task_executor() {
        while(true) {
            queueable_task_t next_task = this->tasks.get_item();
            if (std::holds_alternative <THREAD_SIGNAL>(next_task)) {
                /**
                 * Processor for thread signals.
                 */
                THREAD_SIGNAL signal = std::get <THREAD_SIGNAL>(next_task);
                switch(signal) {
                    case THREAD_SIGNAL::DIE:
                        return;
                }
            } else if (std::holds_alternative <runnable_t>(next_task)) {
                runnable_t runnable_task = std::get<runnable_t>(next_task);
                runnable_task();
            }            
        }
    }
    
    void SimpleThreadPool::thread_executor() {
        this->task_executor();
    }
    
    void SimpleThreadPool::create_threads(usize tcount) {
        std::lock_guard lock(this->pool_mtx);
        to_live_thread_count += tcount;
        
        // Create each thread.
        util::do_n(tcount, [this](){
            // Preinitialise so we can get the actual address of the first object.
            pool.emplace_back(
                std::make_unique<std::atomic<THREAD_STATE>>(THREAD_STATE::UNINITIALIZED), 
                std::make_unique<std::thread>()
            );
            
            // Reference to the thread state. This is only deleted when the pool cleans dead threads,
            // so this thread will already be terminated by the time the reference dangles.
            std::atomic <THREAD_STATE>& thread_state = *(pool.back().first);
            pool.back().second = std::make_unique<std::thread>([&thread_state, this](){
                // We know this will not be destroyed because pool objects are not removed until they are
                // marked dead.
                
                // Start thread.
                {
                    std::lock_guard lock(pool_mtx);
                    currently_alive_thread_count++;
                    thread_state = THREAD_STATE::RUNNING;
                    this->pool_updated.notify_all();
                }
                
                this->thread_executor();
                
                // End of thread
                {
                    std::lock_guard lock(pool_mtx);
                    currently_alive_thread_count--;
                    thread_state = THREAD_STATE::DEAD;
                    this->pool_updated.notify_all();
                }
            });
        });
        
        pool_updated.notify_all();
    }
    
    void SimpleThreadPool::schedule_kill_threads(usize kill_signal_count) {
        std::lock_guard lock(this->pool_mtx);
        to_live_thread_count -= kill_signal_count;
        util::do_n(kill_signal_count, [this](){
            tasks.post_item(queueable_task_t(THREAD_SIGNAL::DIE));
        });
        pool_updated.notify_all();
    }
    
    void SimpleThreadPool::schedule_kill_all_threads() {
        schedule_kill_threads(to_live_thread_count);
    }
    
    void SimpleThreadPool::wait_for_all_threads_to_die() {
        if (currently_alive_thread_count == 0) return;
        // Wait on condition variable
        std::unique_lock lock(pool_mtx);
        pool_updated.wait(lock, [this]() -> bool {
            return currently_alive_thread_count == 0;
        });
    }
    
    void SimpleThreadPool::remove_dead_threads() {
        std::lock_guard lock(pool_mtx);
        pool.remove_if([](pool_thread_t& v) -> bool {
            return *(v.first) == THREAD_STATE::DEAD;
        });
    }
    
    void SimpleThreadPool::wait_for_scheduled_kills() {
        // If we have less alive threads than what there "will" be in the future, then
        // we have done all the killing required to match to_live_thread_count.
        if(to_live_thread_count >= currently_alive_thread_count) return;
        std::unique_lock lock(pool_mtx);
        pool_updated.wait(lock, [this]() -> bool {
            return to_live_thread_count >= currently_alive_thread_count;
        });
    }
    
    SimpleThreadPool::~SimpleThreadPool() {
        this->schedule_kill_all_threads();
        this->wait_for_scheduled_kills();
    }
}
