/**
Copyright 2019 sapient_cogbag <sapient_cogbag@protonmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <crnn/predecl.hpp>
#include <crnn/util.hpp>
#include <numeric>
#include <type_traits>
#include <iostream>


namespace crnn::persistence {
    
    using namespace microtypedefs;
    
    /**
     * All this does is provide some way of splitting raw datatypes from a stream.
     * This is what we should drop to without user defined types.
     * 
     * CRTP
     */
    template <typename Derived>
    class BaseStreamLayer {
        
    public:
        Derived& self() {
            return *static_cast<Derived*>(this);
        }
        
        const Derived& self() const {
            return *static_cast<const Derived*>(this);
        }
        
        /**
         * Read a value from the stream into the result type.
         */
        template <typename IStream, typename ReadValue>
        void read_from_stream(IStream& istream, ReadValue& result) {
            self().read_from_stream(istream, result);
        }
        
        
        /**
         * Write a value to the stream into the result type.
         */
        template <typename OStream, typename WriteValue>
        void write_to_stream(OStream& ostream, const WriteValue& writeval) {
            self().write_to_stream(ostream, writeval);
        }
    };
    
    /**
     * Simply loads and dumps stuff onto a stream.
     */
    class BasicStringStreamLayer : public BaseStreamLayer <BasicStringStreamLayer> {
        
        using stream_format_state_t = std::pair<std::ios_base::fmtflags, std::streamsize>;
        /**
         * Performs the type-specific format operations on a stream, returning the previous format state.
         * Also returns the initial floating point precision. 
         */
        template <typename StreamType, typename ValueType>
        stream_format_state_t apply_type_format(StreamType& iostream, ValueType) {
            std::ios_base::fmtflags initial_fmt = iostream.flags();
            std::streamsize initial_precision = iostream.precision();
            
            //Always show the sign:
            iostream.setf(std::ios_base::showpos);
            
            //Always show the decimal point on floating point types.
            iostream.setf(std::ios_base::showpoint);
            
            //Always show the base prefix
            iostream.setf(std::ios_base::showbase);
            
            // If we are an integral type, enable hex mode
            if constexpr (std::is_integral_v<ValueType>) {
                iostream.setf(std::ios_base::dec, std::ios_base::basefield);
            }
            
            if constexpr (std::is_floating_point_v<ValueType>) {
                // https://en.cppreference.com/w/cpp/io/manip/fixed
                iostream.setf(
                    std::ios_base::fixed,
                    std::ios_base::floatfield
                );
                
                iostream.precision(util::get_full_precision_base10_v<ValueType>);
            }
            return stream_format_state_t{initial_fmt, initial_precision};
        }
        
        /**
         * Restores the given format state to the stream.
         */
        template <typename StreamType>
        void restore_format(StreamType& stream, stream_format_state_t state) {
            stream.flags(state.first);
            stream.precision(state.second);
        }
        
    public:
        template <typename IStream, typename ValueType>
        void read_from_stream(IStream& istream, ValueType& value) {
            // Retrieve current format state and apply type-appropriate modifiers.
            auto initial_fmt = apply_type_format(istream, value);
                        
            // Special case on char to force it to be interpreted as an int so it doesn't break space delimiters.
            if constexpr (util::is_character_type_v <std::decay_t <ValueType>>) {
                i64 actual_decoded_value;
                istream >> actual_decoded_value;
                value = static_cast <std::decay_t <ValueType>>(actual_decoded_value);
            } else {
                istream >> value;
            }
            
            // Restore the format
            restore_format(istream, initial_fmt);
        }
        
        template <typename OStream, typename ValueType>
        void write_to_stream(OStream& ostream, const ValueType& value) {
            auto initial_fmt = apply_type_format(ostream, value);
            if constexpr (util::is_character_type_v <std::decay_t <ValueType>>) {
                i64 actual_encoded_value = value;
                ostream << actual_encoded_value << " ";
            } else {
                ostream << value << " ";
            }
            restore_format(ostream, initial_fmt);
        }
    };
    
    template <typename StreamLayer>
    constexpr bool is_stream_layer_v = std::is_same_v <StreamLayer, BaseStreamLayer<StreamLayer>>;

    
    /**
     * PROTOCOL SPECIFIER PROTOCOL
     * 
     * Simple protocol for defining encoders and their streams.
     * 
     * A PROTOCOL takes a type-to-encode as a type parameter.
     * 
     * If it defines specialises_for_type_v as true then this encoder does specialise for the type.
     * 
     * Else it does not and we can use the type's built-in encode/decode protocol specifier, which is
     * of the name "value_protocol_t" and takes a template template parameter that is the encoder.
     * 
     * If the encoder is specialised for the type T, it must contain the following static methods:
     * 
     * void encode(const T& value_to_encode, StreamLayer& stream_layer, OutputStream& ostream)
     * void decode(T& value_to_decode, StreamLayer& stream_layer, InputStream& istream);
     * 
     */
    template <typename TypeToEncode> 
    struct basic_protocol {
        static constexpr bool specialises_for_type_v = false;
    };
    
        
    /**
     * A struct that automatically generates static encode and decode methods for a protocol at a given type
     * such that it just dumps the value into the StreamLayer without any funky business. Good for quick
     * implementation of the basic types (bool, float, ints, etc.) without needing macros (ewwwww).
     * 
     * Usage:
     * template <>
     * struct your_protocol <type> : simple_stream_dump_t <type>{};
     */
    template <typename ValueType>
    struct simple_stream_dump_t {
        static constexpr bool specialises_for_type_v = true;
        
        template <typename StreamLayer, typename OStream>
        static void encode(const ValueType& to_encode, StreamLayer& stream_layer, OStream& ostream) {
            stream_layer.write_to_stream(ostream, to_encode);
        }
        
        template <typename StreamLayer, typename IStream>
        static void decode(ValueType& to_decode, StreamLayer& stream_layer, IStream& istream) {
            stream_layer.read_from_stream(istream, to_decode);
        }
    };
    
#define BPFT_M(t) template<> struct basic_protocol<t> : simple_stream_dump_t<t>{}
    BPFT_M(i8);
    BPFT_M(i16);
    BPFT_M(i32);
    BPFT_M(i64);
    
    BPFT_M(u8);
    BPFT_M(u16);
    BPFT_M(u32);
    BPFT_M(u64);
    
    BPFT_M(f32);
    BPFT_M(f64);
    BPFT_M(bool);
    
    namespace _detail {
        /**
        * Implementation for the encoding function
        */
        template <
            template <typename> typename protocol, 
            typename ValueType, 
            typename StreamLayer, 
            typename OStream
        >
        struct _impl_encode_t {
            static_assert(is_stream_layer_v<StreamLayer>, "StreamLayer must actually be one.");
            
            static void call(const ValueType& to_encode, StreamLayer& stream_layer, OStream& ostream) {
                if constexpr(!protocol<ValueType>::specialises_for_type_v) {
                    using value_protocol_t = typename std::decay_t<ValueType>::template value_protocol_t<protocol>;
                    value_protocol_t::encode(to_encode, stream_layer, ostream);
                } else {
                    using value_protocol_t = protocol<ValueType>;
                    value_protocol_t::encode(to_encode, stream_layer, ostream);
                }
            }
        };
        
        /**
         * Implementation for the decoding function
         */
        template <
            template <typename> typename protocol, 
            typename ValueType, 
            typename StreamLayer, 
            typename IStream
        >
        struct _impl_decode_t {
            static_assert(is_stream_layer_v<StreamLayer>, "StreamLayer must actually be one.");
            
            static void call(ValueType& to_decode, StreamLayer& stream_layer, IStream& istream) {
                if constexpr(!protocol<ValueType>::specialises_for_type_v) {
                    using value_protocol_t = typename std::decay_t<ValueType>::template value_protocol_t<protocol>;
                    value_protocol_t::decode(to_decode, stream_layer, istream);
                } else {
                    using value_protocol_t = protocol<ValueType>;
                    value_protocol_t::decode(to_decode, stream_layer, istream);
                }
            }
        };
    }
    
    template <template<typename> typename protocol>
    struct protocol_for_type_t {
        /**
         * Encode a type with the given protocol into the given stream using the given stream translation layer.
         */
        template <typename ValueType, typename StreamLayer, typename OStream>
        static void encode(const ValueType& to_encode, StreamLayer& stream_layer, OStream& ostream) {
            _detail::_impl_encode_t<
                protocol, 
                ValueType, 
                StreamLayer, 
                OStream
            >::call(to_encode, stream_layer, ostream);
        }
        
        /**
         * Decode a type with the given protocol from the given stream using the given stream translation layer.
         */
        template <typename ValueType, typename StreamLayer, typename IStream>
        static void decode(ValueType& to_decode, StreamLayer& stream_layer, IStream& istream) {
            _detail::_impl_decode_t<
                protocol,
                ValueType,
                StreamLayer,
                IStream
            >::call(to_decode, stream_layer, istream);
        }
    };
}
