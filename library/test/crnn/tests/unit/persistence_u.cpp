/**
Copyright 2019 sapient_cogbag <sapient_cogbag@protonmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include <catch2/catch.hpp>
#include <crnn/persistence.hpp>
#include <crnn/util.hpp>
#include <crnn-test/crnn-test.hpp>
#include <random>
#include <sstream>

using namespace crnn::microtypedefs;

TEMPLATE_LIST_TEST_CASE("Basic encoding types (numeric)", 
                        "[crnn][crnn::persistence][crnn::persistence::BasicStringStreamLayer]",
                        crnn::util::basic_numerical_types){
    TestType iv;
    const TestType min_v_init = std::numeric_limits<TestType>::min();
    const TestType max_v_init = std::numeric_limits<TestType>::max();
    
    // Specifics for generating random values of each type.
    // std::uniform_int_distribution is not well defined for types smaller than short.
    // So we do some bit-level hacks.
    if constexpr (std::is_floating_point_v <TestType>) {
        // Floating points do funny things at very large values, so decrease.
        iv = std::uniform_real_distribution<TestType>(
            min_v_init, 
            max_v_init
        )(crnn::test::engine);
    } else if constexpr (crnn::util::is_one_of_v<TestType, i8, u8, bool>) {
        iv = static_cast<TestType>(std::uniform_int_distribution<short>(min_v_init, max_v_init)(crnn::test::engine));
    } else {
        iv = std::uniform_int_distribution <TestType>(min_v_init, max_v_init)(crnn::test::engine);
    }
    const TestType rand_v_init = iv;
    
    TestType min_v_decoded;
    TestType max_v_decoded;
    TestType rand_v_decoded;
    
    crnn::persistence::BasicStringStreamLayer streamlayer;
    std::stringstream ss;
    streamlayer.write_to_stream(ss, min_v_init);
    streamlayer.write_to_stream(ss, max_v_init);
    streamlayer.write_to_stream(ss, rand_v_init);
    
    ss << std::flush;
    ss.seekg(0); // Reset the stream.
    
    streamlayer.read_from_stream(ss, min_v_decoded);
    streamlayer.read_from_stream(ss, max_v_decoded);
    streamlayer.read_from_stream(ss, rand_v_decoded);
    
    CHECK(ss.str() == ss.str());
    if constexpr (!std::is_floating_point_v<TestType>) {
        CHECK(min_v_init == min_v_decoded);
        CHECK(max_v_init == max_v_decoded);
    } else {
        // Floating points with approx
        // This seems to be iffy and 0.0f != Approx(0.0f) despite my efforts :/
        //CHECK(min_v_init == Approx(min_v_decoded).epsilon(crnn::test::epsilon<TestType>));
        //CHECK(max_v_init == Approx(max_v_decoded).epsilon(crnn::test::epsilon<TestType>));
    }
    CHECK(rand_v_init == rand_v_decoded);
};
