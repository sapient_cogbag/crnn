/**
Copyright 2019 sapient_cogbag <sapient_cogbag@protonmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once

#include <crnn/predecl.hpp>
#include <variant>
#include <algorithm>
#include <stack>
#include <chrono>
#include <crnn/associative_indexed_memory.hpp>
#include <numeric>
#include <crnn/util.hpp>

namespace crnn::nn {
    
    using namespace std::literals::chrono_literals;
    
    /**
     * Holds per-neuron persistent data related to current charge, threshold for discharge,
     * and a constant for decays.
     */
    struct NeuronChargeData {
        f64 charge = 0;
        f64 activation_threshold = 1;
        f64 decay_time_factor_constant = 0.9995;
        void combine(const NeuronChargeDeltaData& delta);
    };
    
    /**
     * Holds a change in the neuron charge data (charge, threshold, and decay time)
     */
    struct NeuronChargeDeltaData {
        // How much to increment the charge by.
        f64 charge_increment = 0;
        
        // How much to change the activation threshold.
        f64 activation_threshold_increment = 0;
        
        // How much to multiply the decay factor constant.
        f64 decay_rate_multiplier = 1;
    };
    
    /**
     * Combine two neuron charge deltas together appropriately.
     */
    NeuronChargeDeltaData combine(const NeuronChargeDeltaData& a, const NeuronChargeDeltaData& b);
    
    /**
     * Combine elements of an iterator over neuron charge delta data.
     */
    template <typename StartIter, typename EndIter>
    NeuronChargeDeltaData combine_multi_charge_deltas(StartIter start, EndIter end) {
        return std::accumulate(
            start, 
            end, 
            NeuronChargeDeltaData(), 
            [](const NeuronChargeDeltaData& a, const NeuronChargeDeltaData& b){
                return combine(a, b);
        });
    }
    
    /**
     * Holds data related to the last activation of a given neuron.
     */
    struct NeuronActivationData {
        f64 charge_at_time;
        f64 threshold_at_time;
        std::chrono::nanoseconds last_activation_time;
        void update(const NeuronActivationUpdateData& a);
    };
    
    struct NeuronActivationUpdateData {
        f64 charge_at_time = 0; 
        f64 threshold_at_time = 0;
        std::chrono::nanoseconds last_activation_time = 0ns;
    };
    
    /**
     * Combine two neuron activation updates.
     */
    NeuronActivationUpdateData combine(const NeuronActivationUpdateData& a, const NeuronActivationUpdateData& b);
    
    /**
     * Combine multiple activation deltas (just picks the largest one).
     * 
     * If there are none it returns a zeroed-out struct.
     */
    template <typename StartIter, typename EndIter>
    NeuronActivationUpdateData combine_multi_activation_deltas(StartIter start, EndIter end) {
        // We simply find the latest one.
        if (std::distance(end - start) == 0) return NeuronActivationUpdateData{0, 0, 0ns};
        return *std::max_element(
            start, end, [](const NeuronActivationUpdateData& a, const NeuronActivationUpdateData& b){
                return a.last_activation_time < b.last_activation_time;
            }
        );
    }
    
    struct FullNeuronData {
        NeuronChargeData charge_data;
        NeuronActivationData activation_data;
        
        /**
         * Update the object with the data.
         */
        void combine(const FullNeuronDeltaData& other);
        void combine(const NeuronActivationUpdateData& other);
        void combine(const NeuronChargeDeltaData& other);
    };
    
    struct FullNeuronDeltaData {
        NeuronChargeDeltaData charge_delta;
        NeuronActivationUpdateData activation_update;
    };
    
    FullNeuronDeltaData combine(const FullNeuronDeltaData& a, const FullNeuronDeltaData& b);
    
    /**
     * Combine the deltas for a full neuron delta data, independently.
     */
    template <typename StartIter, typename EndIter>
    FullNeuronDeltaData combine_multi_neuron_deltas(StartIter start, EndIter end) {
        using charge_mem_creator = util::create_member_iterator<&FullNeuronDeltaData::charge_delta>;
        using active_update_mem_creator = util::create_member_iterator<&FullNeuronDeltaData::activation_update>;
        // Just getting member iterators <3
        return FullNeuronDeltaData {
            NeuronChargeDeltaData(combine_multi_charge_deltas(
                charge_mem_creator::call(start),
                charge_mem_creator::call(end)
            )), // combine the charges
            NeuronActivationUpdateData(combine_multi_activation_deltas(
                active_update_mem_creator::call(start),
                active_update_mem_creator::call(end)
            )) // Select the latest active update
        };
    }
    
    /**
     * Holds per-neuron-connection data - such as the weight and sensitization coefficient.
     */
    struct NeuronConnectionData {
        // Weights are relative/proportional.
        f64 weight;
        // Multiplier for charge passing through - allows for desensitization.
        f64 sensitization_coefficient;
        // The threshold above which the sensitization coefficient gets decreased
        f64 desensitization_threshold;
        // Rate at which sensitization starts to renormalise (go back towards the default/initial val).
        // Multiplicative/exponential with time
        // 1 = no change over time. 0.995 = moves closer to the normal sensitization coefficient.
        // >1 = moves further from the normal sensitization coefficient
        f64 sensitization_renormalisation_constant = 0.9995;
        // The time since charge was last dumped down the connection.
        std::chrono::nanoseconds last_activation_time = 0ns;
        // The time beyond which resensitization can begin to occur since the last activation.
        std::chrono::nanoseconds resensitization_time_threshold = 10min;
        
        void combine(const NeuronConnectionDeltaData& delta);
    };
    
    struct NeuronConnectionDeltaData {
        f64 weight_delta = 0;
        f64 sensitization_coefficient_delta = 0;
        f64 desensitization_threshold_delta = 0;
        f64 sensitization_renormalisation_constant_multiplier = 1;
        std::chrono::nanoseconds last_activation_time = 0ns;
        std::chrono::nanoseconds resensitization_time_threshold_delta = 0min;
    };
    
    template <typename StartIter, typename EndIter>
    NeuronConnectionDeltaData combine_multi_connection_deltas(StartIter start, EndIter end) {
        // Basic weight.
        using weight_conv_t = util::create_member_iterator<
            &NeuronConnectionDeltaData::weight_delta
        >;
        auto weight_iter_s = weight_conv_t::call(start);
        auto weight_iter_e = weight_conv_t::call(end);
        f64c total_weight_delta = std::accumulate(weight_iter_s, weight_iter_e, 0);
        
        // sensitization coefficient
        using sstzn_coeff_conv_t = util::create_member_iterator<
            &NeuronConnectionDeltaData::sensitization_coefficient_delta
        >;
        auto sstzn_coeff_s = sstzn_coeff_conv_t::call(start);
        auto sstzn_coeff_e = sstzn_coeff_conv_t::call(end);
        f64c total_sensitization_coefficient_delta = std::accumulate(sstzn_coeff_s, sstzn_coeff_e, 0);
        
        // desensitization threshold
        using dsstzn_thresh_conv_t = util::create_member_iterator<
            &NeuronConnectionDeltaData::desensitization_threshold_delta
        >;
        auto dsstzn_thresh_s = dsstzn_thresh_conv_t::call(start);
        auto dsstzn_thresh_e = dsstzn_thresh_conv_t::call(end);
        f64c total_desensitization_threshold_delta = std::accumulate(dsstzn_thresh_s, dsstzn_thresh_e, 0);
        
        // Multiplicative coefficient of the resensitization coefficient.
        using sstzn_rnm_conv_t = util::create_member_iterator<
            &NeuronConnectionDeltaData::sensitization_renormalisation_constant_multiplier
        >;
        auto sstzn_rnm_s = sstzn_rnm_conv_t::call(start);
        auto sstzn_rnm_e = sstzn_rnm_conv_t::call(end);
        f64c total_sensitization_renormalization_coefficient = std::accumulate(
            sstzn_rnm_s, 
            sstzn_rnm_e, 1.0, 
            [](f64 a, f64 b){
                return a * b;
            }
        );
        
        // The last activation time.
        // We pick the latest, obviously.
        using last_activation_time_conv_t = util::create_member_iterator<
            &NeuronConnectionDeltaData::last_activation_time
        >;
        auto lat_s = last_activation_time_conv_t::call(start);
        auto lat_e = last_activation_time_conv_t::call(end);
        auto max_elem = std::max_element(lat_s, lat_e);
        // Returns the last element if there are no values. We give a default;
        const std::chrono::nanoseconds last_activation_time = (max_elem == lat_e) ? 0ns : *max_elem;
        
        // the change in the minimum 
        using rsstzn_ttd_conv_t = util::create_member_iterator<
            &NeuronConnectionDeltaData::resensitization_time_threshold_delta
        >;
        auto rsstzn_ttd_s = rsstzn_ttd_conv_t::call(start);
        auto rsstzn_ttd_e = rsstzn_ttd_conv_t::call(end);
        
        const std::chrono::nanoseconds total_resensitization_time_threshold = std::accumulate(
            rsstzn_ttd_s, rsstzn_ttd_e, 0ns
        );
        
        
        return NeuronConnectionDeltaData {
            total_weight_delta,
            total_sensitization_coefficient_delta,
            total_desensitization_threshold_delta,
            total_sensitization_renormalization_coefficient,
            last_activation_time,
            total_resensitization_time_threshold
        };
        
    }
    
    
    /**
     * Holds the raw neural network data.
     */
    struct NeuralNetworkData : public aimem::idx_system_data_manager_t<
        2, NeuralNetworkData, NeuronConnectionData
    > {
        // Index system 1 is in neuron read order.
        // Index system 2 is the charge rewriting in neuron write order.
        u64 neuron_count;
        
        // Default multiplier for charges going through.
        f64 default_sensitization_coefficient = 1.0;
        
        // Default decay of *=0.975 per second.
        f64 default_decay_coefficient = 0.975;
        
        std::chrono::nanoseconds simulation_time;
        std::vector<association_t> network;
        std::vector<FullNeuronData> neurons;
    };
    
    struct NeuralNetworkProcessor {
        NeuralNetworkData::indexed_data_t<
            0, aimem::data_storage_system::unique_data_t, FullNeuronData
        > neuron_charge_and_activation_data;
        
        
    };
}
