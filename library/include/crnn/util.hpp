/**
Copyright 2019 sapient_cogbag <sapient_cogbag@protonmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <crnn/predecl.hpp>
#include <ios>
#include <optional>
#include <functional>
#include <iostream>
#include <vector>

namespace crnn::util {
    /**
     * Simply does nothing to any of it's arguments.
     */
    constexpr auto null_function = [](auto... a){};
    
    /**
     * Simple type list type.
     */
    template <typename... T>
    struct type_list;
    
    using basic_numerical_types = type_list<
        bool, 
        f32, f64, 
        i8, i16, i32, i64, 
        u8, u16, u32, u64
    >;
    
    using basic_character_types = type_list<
        signed char, unsigned char, char,
        char16_t, char32_t, wchar_t //TODO: Add char8_t in C++20
    >;
    
    template <typename T, template <typename...> typename TMP>
    struct is_template_instantiation {
        static constexpr bool value = false;
    };
    
    template <typename... _Params, template <typename...> typename TMP>
    struct is_template_instantiation <TMP<_Params...>, TMP> {
        static constexpr bool value = true;
    };
    
    template <typename T, template <typename...> typename TMP>
    constexpr bool is_template_instantiation_v = is_template_instantiation<T, TMP>::value;
    
    /**
     * Run a function @param count times and pass the index as an argument. 
     * The function should take the current index in the iteration.
     */
    template <typename Callable>
    void do_n_indexed(u64 count, Callable&& run) {
        for (u64 i = 0; i < count; i++) {
            run(i);
        }
    }
    
    
    /**
     * Run a function @param count times. The function should take no arguments.
     */
    template <typename Callable>
    void do_n(u64 count, Callable&& run) {
        do_n_indexed(count, [&](u64){
            run();
        });
    }
    
    
    /**
     * Run a function over the indices provided, the function should take
     * an argument of the index. The end is not included. If start >= end the
     * function is not ever run.
     */
    template <typename Callable>
    void do_range_indexed(u64 start, u64 end, Callable&& run) {
        if(start>=end) return;
        u64c delta = end - start;
        do_n_indexed(delta, [start, run](u64c idx){
            run(idx + start);
        });
    }
    
    /**
     * Perform a parameterless callable over a range. Does the function 0 times
     * if end <= start.
     */
    template <typename Callable>
    void do_range(u64 start, u64 end, Callable&& run) {
        do_range_indexed(start, end, [run](u64c idx){
            run();
        });
    }
        
        
    template <template <typename> typename Criterion>
    struct criterion {
        template <typename... T>
        constexpr static bool all_satisfy_v = (Criterion<T>::value&&...&&true);

        template <typename... T>
        struct all_satisfy{
            constexpr static bool value = all_satisfy_v<T...>;
        };
        
        template <typename... T>
        constexpr static bool one_satisfies_v = (Criterion<T>::value||...||false);
        
        template <typename... T>
        struct one_satisfies {
            constexpr static bool value = one_satisfies_v<T...>;
        };
    };
    
    
    /**
     * Create a template tester that takes in one type and results in a template that takes a
     * parameter and tests if it is the same type.
     */
    template <typename T>
    struct is_t {
        template <typename Q>
        struct result {
            static constexpr bool value = std::is_same_v <Q, T>;
        };
        
        template <typename Q>
        using result_tt = result<Q>;
    };
    
    /**
     * Get if a type is in a set of other types.
     */
    namespace _detail {
        template <typename _TestType, typename... _Others>
        struct _is_one_of {
            static constexpr bool value = 
                criterion <is_t<_TestType>::template result_tt>::
                template one_satisfies_v<_Others...>;
        };
    }
    
    template <typename TestType, typename... TypeSet>
    struct is_one_of {
        static constexpr bool value = _detail::_is_one_of<TestType, TypeSet...>::value;
    };
    
    /**
     * Checks whether a type is one of a set of values.
     */
    template <typename TestType, typename... TypeSet>
    constexpr bool is_one_of_v = is_one_of<TestType, TypeSet...>::value;
    
    /**
     * implementing the typelist class
     */
    template <typename... T>
    struct type_list {
        template <typename Q>
        static constexpr bool contains_v = is_one_of_v<Q, T...>;
    };
    
    
    /**
     * Is the type a character type => see https://en.cppreference.com/w/cpp/language/types
     */
    template <typename TestType>
    constexpr bool is_character_type_v = basic_character_types::contains_v<TestType>;
    
    /**
     * Gets the precision specifier (for use with stream::setprecision) to fully represent a FloatingPointType
     * in a stream.
     * 
     * We know base 16 gives more compact numbers than base 10, so we just use base 10 (so we don't have to know the
     * hardware-specific radix and screw with custom hex/oct/bin type formatting).
     * 
     * The base 10 value is just the same.
     * 
     * See: https://en.cppreference.com/w/cpp/io/manip/setprecision
     */
    template <typename FloatingPointType>
    struct get_full_precision_base {
        static_assert(
            std::is_floating_point_v<FloatingPointType>, "This template only makes sense on floating-point");
        static constexpr std::streamsize value16 = std::numeric_limits<FloatingPointType>::digits10 + 1;
        static constexpr std::streamsize value10 = std::numeric_limits<FloatingPointType>::digits10 + 1;
    };
    
    template <typename FloatingPointType>
    constexpr std::streamsize get_full_precision_base16_v = get_full_precision_base <FloatingPointType>::value16;
    
    template <typename FloatingPointType>
    constexpr std::streamsize get_full_precision_base10_v = get_full_precision_base <FloatingPointType>::value10;
    
    /**
     * For calling a function with the next @tparam N elements in the container with the associated iterator.
     * 
     * Undefined if N would go out of range.
     * 
     * Also allows passing of arguments before passing the various values from the iterator.
     */
    template <usize N>
    struct call_with_n_next_elements {
    private:
        template <
            usize... sequential_delta_ints, // The ints from 0 to N-1 in order.
            typename IteratorType,
            typename ProcessorFunction,
            typename... PreArgs
        > 
        static auto _impl(
            std::index_sequence<sequential_delta_ints...>, 
            IteratorType start,
            ProcessorFunction&& process,
            PreArgs&&... pre_args
        ) {
            return std::invoke(
                std::forward<ProcessorFunction>(process), 
                std::forward<PreArgs>(pre_args)..., 
                *(start + sequential_delta_ints)...
            );
        }
        
        
    public:
        template <typename IteratorType, typename ProcessorFunction, typename... PreArgs>
        static auto call(IteratorType&& start_iter, ProcessorFunction&& process, PreArgs&&... pre_args) {
            return _impl(
                std::make_index_sequence<N>(), 
                std::forward<IteratorType>(start_iter),
                std::forward<ProcessorFunction>(process),
                std::forward<PreArgs>(pre_args)...
            );
        }
        
    };
    
    /**
     * Utility class that allows us to default construct nullopt_t with std::nullopt
     */
    template <typename T>
    struct default_construct_t {
        
        default_construct_t(){}
        
        operator T () {
            return T();
        }
    };
    
    template <>
    struct default_construct_t <std::nullopt_t> {
        default_construct_t(){}
        operator std::nullopt_t () {
            return std::nullopt;
        }
    };
    
    constexpr auto identity_function = [](auto&& v) -> auto{
        return std::forward<decltype(v)>(v);
    };
    
    /**
     * Extracts the start indices in the given iterable range where the return value
     * of the processing function when applied to the given index changes.
     * This means the first element of the returned vector is 0 if there are any values.
     * 
     * Optional "estimated element count" parameter can provide an estimate of the upper bound
     * of the total number of points of change, to enable more efficient memory allocation. If it's zero we
     * just don't do allocation.
     * 
     * @param processor can be any invokable on the elements of the iterable object 
     * (including pointer-to-member and such)
     */
    template <typename StartIter, typename EndIter, typename ProcessorFunction = const decltype(identity_function)&>
    inline std::vector <usize> locations_of_change(
        StartIter start, 
        EndIter end, 
        ProcessorFunction&& processor = identity_function, 
        usize estimated_count = 0
    ) {
        using _comp_type_t = std::invoke_result_t<
            decltype(std::forward<ProcessorFunction>(processor)), 
            decltype(*start)
        >;
        // If result is a reference, do some screwery to avoid weird errors about std::optional.
        using comp_type_t = std::remove_reference_t<_comp_type_t>;
        
        std::vector<usize> result;
        if (estimated_count > 0) result.reserve(estimated_count);
        
        std::optional<comp_type_t> last_value(std::nullopt);
        u64 delta = 0;
        std::optional<comp_type_t> current_value(std::nullopt);
        
        while (start + delta < end) {
            
            auto&& v = *(start + delta);
            
            current_value = std::invoke(std::forward<ProcessorFunction>(processor), std::forward<decltype(v)>(v));
            if (current_value != last_value) {
                result.push_back(delta);
                last_value = current_value;
            }
            delta++;
        }
        
        return result;
    }
    
    /**
     * Get a pair of iterators from the given data set indicating the start and end of
     * the nth data chunk where the predicate used to generate the provided locations of
     * change was all equal. If @param n is larger than the number of chunks, behaviour is undefined.
     */
    template <typename StartIter, typename EndIter>
    inline auto get_nth_data_block(
        StartIter data_start, 
        EndIter data_end, 
        const std::vector<usize>& blocks,
        usize n
    ) {
        u64c start_idx = blocks[n];
        // Last block?
        u64c end_idx = (blocks.size() == n+1) ? data_end - data_start : blocks[n + 1];
        
        return std::pair(data_start + start_idx, data_start + end_idx);
    }
    
    /**
     * Element transform. Turn an iterator of objects into one that gets elements from the object.
     * 
     * Only works on iterators with random access or greater manouverability..
     */
    template <typename Clazz, typename MemberType, MemberType Clazz::* member, typename InternalIterator>
    struct member_iterator_t : std::iterator <
        typename std::iterator_traits <InternalIterator>::iterator_category,
        std::invoke_result_t<decltype(member), decltype(*InternalIterator())>
    > {
        using FullMemberType = MemberType Clazz::*;
        using difference_type = typename std::iterator_traits<InternalIterator>::difference_type;
        
    private:
        InternalIterator internal;
        
    public:
        member_iterator_t& operator=(member_iterator_t other) {
            internal = other.internal;
            return *this;
        }
        
        MemberType& operator*() {
            return std::invoke(member, *internal);
        }
        
        const MemberType& operator*() const {
            return std::invoke(member, *internal);
        }
        
        MemberType* operator->() {
            return std::addressof(**this);
        }
        
        const MemberType* operator->() const {
            return std::addressof(**this);
        }
        
        
        member_iterator_t& operator+=(difference_type other) {
            internal += other;
            return *this;
        }
        
        member_iterator_t& operator-=(difference_type other) {
            (*this) += -other;
            return *this;
        }
        
        
        member_iterator_t operator+(difference_type other) const {
            member_iterator_t result(internal);
            result += other;
            return result;
        }
        
        // Allows for lookup outside this template namespace. (see: https://en.cppreference.com/w/cpp/language/adl)
        friend member_iterator_t operator+(difference_type lhs, const member_iterator_t& rhs) {
            return rhs + lhs;
        }

        member_iterator_t operator-(difference_type other) const {
            member_iterator_t result(internal);
            result -= other;
            return result;
        }
        
        difference_type operator-(const member_iterator_t& other) const {
            return this->internal - other.internal;
        }
        
        MemberType& operator[] (difference_type n) {
            return *(*this + n);
        }
        
        const MemberType& operator[] (difference_type n) const {
            return *(*this + n);
        }
        
        bool operator==(const member_iterator_t& other) const {
            return internal == other.internal;
        }
        
        bool operator!=(const member_iterator_t& other) const {
            return internal != other.internal;
        }
        
        bool operator<(const member_iterator_t& other) const {
            return internal < other.internal;
        }
        
        bool operator<=(const member_iterator_t& other) const {
            return internal <= other.internal;
        }
        
        bool operator>(const member_iterator_t& other) const {
            return internal > other.internal;
        }
        
        bool operator >=(const member_iterator_t& other) const {
            return internal >= other.internal;
        }
        
        
        // Prefix
        member_iterator_t& operator++() {
            *this += 1;
            return *this;
        }
        
        // Postfix
        member_iterator_t operator++(int) {
            member_iterator_t old_v(*this);
            (*this)++;
            return old_v;
        }
        
        //Prefix
        member_iterator_t& operator--() {
            *this -= 1;
        }
        
        //Postfix
        member_iterator_t operator--(int) {
            member_iterator_t old_v(*this);
            (*this)--;
            return old_v;
        }
    };
    
    template <auto member>
    struct create_member_iterator;
    
    template <typename Clazz, typename MemberType, MemberType Clazz::* member>
    struct create_member_iterator<member> {
        template <typename InternalIterator>
        using result_t = member_iterator_t<Clazz, MemberType, member, InternalIterator>;
        
        template <typename InternalIterator>
        static result_t<InternalIterator> call(InternalIterator iter) {
            return result_t<InternalIterator>(iter);
        }
    };

    
    
    /**
     * Print a std::vector to the stream using the stream operator.
     * 
     * Can pass true or false as last argument to put a newline after each element.
     * 
     * Provides indent via a preline option - the characters to put before each line.
     */
    template <typename T, typename... _Rest>
    void printvec(
        std::ostream& o, 
        const std::vector<T, _Rest...>& vec, 
        bool newline_each_item=true, 
        const std::string& preline=""
    ) {
        const std::string endchunk = newline_each_item ? "\n" : " "; // Spaces or newlines after commas.
        const std::string indent = "    ";
        o << preline << "{" << (newline_each_item ? "\n" : "");
        usizec input_vec_length = vec.size();
        
        do_n_indexed(input_vec_length, [&](u64c idx){
            const T& output_item = vec[idx];
            if constexpr (is_template_instantiation_v<T, std::vector>) { // Use this function for subvectors too.
                printvec(o, output_item, newline_each_item, preline + indent); // Use preline to create indent.
            } else {
                o << preline << indent << output_item;
            }
            if (idx != input_vec_length-1) { // Not last.
                o << ",";
            }
            o << endchunk;
        });
        o << preline << "}";
    }
    
    /**
     * A type which allows you to mark dirty/clean/processed/unprocessed.
     * 
     * Works on trivially copy constructible types. Does NOT do equals check on assignment and will always mark
     * as "to process"
     */
    template <typename T>
    struct monoprocessable {
        T data;
        bool processed = false;
        
        
        /**
         * Retrieve the value and mark as processed.
         */
        T get_for_processing() {
            processed = true;
            return data;
        }
        
        bool should_process() const {
            return !processed;
        }
        
        void mark_processed() {
            processed = true;
        }
        
        monoprocessable& operator=(T d) {
            data = d;
            mark_processed();
            return *this;
        }
        
        /**
         * Call an invokeable on the object if it is processed.
         * 
         * Discards the result. If you want to process the result, then provide a callable that takes
         * a forwarded reference to the result type.
         * 
         * Note that if the ConditionalProcessor has a return type of void, the other processor is unused.
         */
        template <typename ConditionalProcessor, typename ResultProcessor = const decltype(null_function)&>
        void call_if_unprocessed(const ConditionalProcessor&& proc, const ResultProcessor&& result_proc = null_function) {
            if (should_process()){
                if constexpr(!std::is_same_v<std::invoke_result_t<decltype(proc), T&>, void>) {
                    // not void
                    auto&& res = std::invoke(std::forward<ConditionalProcessor>(proc), data);
                    std::invoke(std::forward<ResultProcessor>(result_proc), std::forward<decltype(res)>(res));
                } else {
                    std::invoke(std::forward<ConditionalProcessor>(proc), data);
                }
            }
        } 
    };
    
    /**
     * A type which calls an arbitrary function in it's destructor. Effectively forces something to be
     * done regardless of exit point.
     */
    template <typename DestructorFunction>
    struct unconditionally_on_exit {
        const DestructorFunction a;
        unconditionally_on_exit(DestructorFunction a) : a(a){};
        ~unconditionally_on_exit(){
            std::invoke(a);
        }
    };
    
    template <typename DestructorFunction>
    unconditionally_on_exit(DestructorFunction a) -> unconditionally_on_exit<DestructorFunction>;
    
    /**
     * Holds a proxy to buffer data in a std::vector.
     * 
     * Does not let you resize, but does let you do most other things.
     */
    template <typename T, typename _Alloc = std::allocator<T> >
    class buffer_proxy {
    protected:
        using buf_t = std::vector <T, _Alloc>;
        buf_t& buf;
        
    public:
        
        // Mirror std::vector typedefs.
        // See: https://en.cppreference.com/w/cpp/container/vector
        using value_type = typename buf_t::value_type;
        using allocator_type = typename buf_t::allocator_type;
        using size_type = typename buf_t::size_type;
        using difference_type = typename buf_t::difference_type;
        
        using reference = typename buf_t::reference;
        using const_reference = typename buf_t::const_reference;
        using pointer = typename buf_t::pointer;
        using const_pointer = typename buf_t::const_pointer;
        
        using iterator = typename buf_t::iterator;
        using const_iterator = typename buf_t::const_iterator;
        using reverse_iterator = typename buf_t::reverse_iterator;
        using const_reverse_iterator = typename buf_t::const_reverse_iterator;
        
        buffer_proxy(buf_t& buf) : buf(buf){}
        buffer_proxy(const buffer_proxy& a) : buf(a.buf) {}
        
        /**
         * Get a constref to the data for this buffer proxy (can't be modified tho, use the proxy methods)
         */
        const buf_t& get_buf() const {
            return buf;
        }
        
        // BEGIN Iterators
        iterator begin() noexcept {
            return buf.begin();
        }
        
        const_iterator begin() const noexcept {
            return buf.begin();
        }
        
        const_iterator cbegin() const noexcept {
            return buf.cbegin();
        }
        
        iterator end() noexcept {
            return buf.end();
        }
        
        const_iterator end() const noexcept {
            return buf.end();
        }
        
        const_iterator cend() const noexcept {
            return buf.cend();
        }
        // END
        
        // BEGIN Reverse iterators
        reverse_iterator rbegin() noexcept {
            return buf.rbegin();
        }
        
        const_reverse_iterator rbegin() const noexcept {
            return buf.rbegin();
        }
        
        const_reverse_iterator crbegin() const noexcept {
            return buf.crbegin();
        }
        
        reverse_iterator rend() noexcept {
            return buf.rend();
        }
        
        const_reverse_iterator rend() const noexcept {
            return buf.rend();
        }
        
        const_reverse_iterator crend() const noexcept {
            return buf.crend();
        }
        // END
        
        // BEGIN Data access
        reference at(size_type s) {
            return buf.at(s);
        }
        
        const_reference at(size_type s) const {
            return buf.at(s);
        }
        
        reference operator[](size_type s) {
            return buf[s];
        }
        
        const_reference operator[](size_type s) const {
            return buf[s];
        }
        
        reference front() {
            return buf.front();
        }
        
        const_reference front() const {
            return buf.front();
        }
        
        reference back() {
            return buf.back();
        }
        
        const_reference back() const {
            return buf.back();
        }
        
        T* data() noexcept {
            return buf.data();
        }
        
        const T* data() const noexcept {
            return buf.data();
        }
        // END
        
        // BEGIN Capacity
        [[nodiscard]] bool empty() const noexcept {
            return buf.empty();
        }
        
        size_type size() const noexcept {
            return buf.size();
        }
        
        size_type max_size() const noexcept {
            return buf.max_size();
        }
        
        size_type capacity() const noexcept {
            return buf.capacity();
        }
        
        // END
    };
    
#define CRNN_DEFINE_BUF_PROXY_OP(op)\
    template <typename T, typename... _Rest>\
    inline bool operator op(buffer_proxy<T, _Rest...> a, buffer_proxy<T, _Rest...> b)\
    {return a.get_buf() op b.get_buf();}\
    template <typename T, typename... _Rest>\
    inline bool operator op(const std::vector<T, _Rest...>& a, buffer_proxy<T, _Rest...> b)\
    {return a op b.get_buf();}\
    template <typename T, typename... _Rest>\
    inline bool operator op(buffer_proxy<T, _Rest...> a, const std::vector<T, _Rest...>& b)\
    {return a.get_buf() op b;}
    
    CRNN_DEFINE_BUF_PROXY_OP(==)
    CRNN_DEFINE_BUF_PROXY_OP(!=)
    CRNN_DEFINE_BUF_PROXY_OP(<)
    CRNN_DEFINE_BUF_PROXY_OP(<=)
    CRNN_DEFINE_BUF_PROXY_OP(>)
    CRNN_DEFINE_BUF_PROXY_OP(>=)

}
