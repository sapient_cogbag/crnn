/**
Copyright 2019 sapient_cogbag <sapient_cogbag@protonmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include <catch2/catch.hpp>
#include <crnn/io/triple_buffer.hpp>
#include <crnn-test/crnn-test.hpp>

using namespace crnn::microtypedefs;

TEST_CASE("Triple buffer testing", "[crnn][crnn::util]") {
    
    using namespace crnn;
    static constexpr u64 cnt = 20;
    util::triple_buffer<i64> data_buf(cnt);
    
    std::vector<i64> values;
    util::do_n(cnt, [&](){
        values.push_back(test::randint());
    });
    
    // No weird threading bullshit here, so the write buffer should be swapped twice, trivially.
    bool has = data_buf.start_writing();
    REQUIRE(has);
    
    if (has) {
        auto write_buffer = data_buf.get_write_buffer();
        std::copy(values.begin(), values.end(), write_buffer.begin());
        bool did_swap = data_buf.done_writing();
        REQUIRE(did_swap);
    }
    
    // Acquire display buffer.
    bool did_swap_complete_buffer = data_buf.start_displaying();
    REQUIRE(did_swap_complete_buffer);
    
    if (did_swap_complete_buffer) {
        auto display_buffer = data_buf.get_display_buffer();
        REQUIRE(display_buffer == values);
        data_buf.done_displaying();
    }
}
