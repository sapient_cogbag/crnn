/**
Copyright 2019 sapient_cogbag <sapient_cogbag@protonmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <crnn/predecl.hpp>
#include <variant>
#include <crnn/util.hpp>
#include <array>
#include <memory>
#include <algorithm>
#include <cmath>
#include <vector>
#include <atomic>

namespace crnn::aimem {
    struct idx_data_storage_system_t {
        // For raw access.
        const IDX_SYSTEM_TYPE idx_system_type;
        const usize elements_per_index;
        
        /**
         * For DYNAMIC_SIZE systems, the second argument is irrelevant.
         * For UNIQUE systems, the second argument is also irrelevant.
         */
        constexpr idx_data_storage_system_t (const IDX_SYSTEM_TYPE type, const usize elements_per_index = 0): 
            idx_system_type(type), 
            elements_per_index(
                type == IDX_SYSTEM_TYPE::DYNAMIC_SIZE? // Auto-select valid element counts.
                0: (type == IDX_SYSTEM_TYPE::UNIQUE ? 1 : elements_per_index)
            ) {}
            
        /**
         * Get if the number of elements for each index is dynamically controlled at runtime.
         */
        constexpr bool is_dynamic() const {
            return idx_system_type == IDX_SYSTEM_TYPE::DYNAMIC_SIZE;
        }
        
        /**
         * Get if the data index system is a 1-to-1 mapping between indices and data elements.
         */
        constexpr bool is_unique() const {
            return (idx_system_type == IDX_SYSTEM_TYPE::UNIQUE) || (elements_per_index == 1);
        }
        
        
        /**
         * Only produces a meaningful number if the system type is not DYNAMIC_SIZE
         * 
         * Get the number of data elements associated with each index.
         */
        constexpr usize get_element_count() const {
            return elements_per_index;
        }
        
        constexpr bool operator==(const idx_data_storage_system_t other) {
            if(other.is_unique() and is_unique()) return true;
            if(other.is_dynamic() and is_dynamic()) return true;
            return other.get_element_count() == get_element_count();
        }
    };
    
    /**
     * A typeset to ease compile problems with preliminary C++20 features.
     * 
     * This set acts as type tags for data storage systems.
     */
    namespace data_storage_system {
        
        namespace _detail {
            
            // Turns a size N into either IDX_SYSTEM_TYPE::FIXED_SIZE or IDX_SYSTEM_TYPE::UNIQUE
            template <usize N>
            struct get_fixed_or_unique_idx_system_type {
                static constexpr IDX_SYSTEM_TYPE value = IDX_SYSTEM_TYPE::FIXED_SIZE;
            };
            
            template <>
            struct get_fixed_or_unique_idx_system_type<1> {
                static constexpr IDX_SYSTEM_TYPE value = IDX_SYSTEM_TYPE::UNIQUE;
            };
            
            template <usize N>
            constexpr IDX_SYSTEM_TYPE 
                fixed_or_unique_idx_system_type_v = get_fixed_or_unique_idx_system_type<N>::value;
        }
        
        // For fixed width per index data
        template <usize N>
        struct fixed_width_data_t {
            static constexpr IDX_SYSTEM_TYPE idx_system_type_v = _detail::fixed_or_unique_idx_system_type_v<N>;
            static constexpr usize elements_per_idx_v = N;
        };
        
        using unique_data_t = fixed_width_data_t<1>;
        
        struct dynamic_width_data_t {
            static constexpr IDX_SYSTEM_TYPE idx_system_type_v = IDX_SYSTEM_TYPE::DYNAMIC_SIZE;
            static constexpr usize elements_per_idx_v = 0;
        };
        
        /**
         * Provides a is_data_width_type_v member (which tells if the tag type is actually a data width specifier type)
         * 
         * If it is, members providing information about the type - dynamic types just get the idx_system_type_v
         * and elements_per_idx_v zero.
         * 
         * fixed width types get the elements per idx and idx system type.
         * 
         * All valid tag types get a value member that is an idx_data_storage_system_t
         */
        template <typename TagType>
        struct info {
            static constexpr bool is_data_width_type_v = false;
        };
        
        template <usize N>
        struct info <fixed_width_data_t<N>> {
            static constexpr bool is_data_width_type_v = true;
            
            static constexpr IDX_SYSTEM_TYPE idx_system_type_v = fixed_width_data_t<N>::idx_system_type_v;
            static constexpr usize elements_per_idx_v = fixed_width_data_t<N>::elements_per_idx_v;
            
            static constexpr idx_data_storage_system_t value = idx_data_storage_system_t(
                idx_system_type_v, elements_per_idx_v
            );
        };
        
        template <>
        struct info <dynamic_width_data_t> {
            static constexpr bool is_data_width_type_v = true;
            
            static constexpr IDX_SYSTEM_TYPE idx_system_type_v = dynamic_width_data_t::idx_system_type_v;
            static constexpr usize elements_per_idx_v = dynamic_width_data_t::elements_per_idx_v;
            
            static constexpr idx_data_storage_system_t value = 
                idx_data_storage_system_t(IDX_SYSTEM_TYPE::DYNAMIC_SIZE);
        };
        
    }
    
    /**
     * Holds the current ordering of an index association data.
     * 
     * This holds precisely two indices in the sorting priority - the first priority one
     * is ordered such that reading from this index system. The second priority one is so
     * that writing to that index system will be less un-sequential if indices in the first
     * and second priority systems are roughly associated together. Other index systems
     * can be in any order and are not accounted for by the type system.
     */
    struct index_association_storage_order {
        const usize priority_ordered_idx_system;
        const usize second_priority_ordered_idx_system;
        
        constexpr index_association_storage_order(usize pois, usize spois) : 
        priority_ordered_idx_system(pois),
        second_priority_ordered_idx_system(spois)
        {}
        
        constexpr bool operator==(const index_association_storage_order other) const {
            return priority_ordered_idx_system == other.priority_ordered_idx_system
                && second_priority_ordered_idx_system == other.second_priority_ordered_idx_system;
        }
        
        constexpr bool operator!=(const index_association_storage_order other) const {
            return !(*this == other);
        }
    };
    
    /**
     * Holds tag types for the ordering of data holding associations between indices.
     * 
     * The first priority order is the index system to order by first.
     * If two index associations are for the same index in the first priority system, then we order by the second
     * priority.
     * 
     * If the first and second priority order are the same then we only order by one index. mono_ordering_v
     * is true in that case.
     */
    namespace idx_association_storage {
        template <usize first_priority, usize second_priority = first_priority>
        struct idx_association_storage_order_t {
            static constexpr usize first_priority_order_v = first_priority;
            static constexpr usize second_priority_order_v = second_priority;
            
            static constexpr bool is_mono_ordering_v = (first_priority == second_priority);
        };
        
        /**
         * Holds the info for the index association ordering tag type.
         */
        template <typename TagType>
        struct info {
            static constexpr bool is_idx_association_storage_order_v = false;
        };
        
        template <usize first_priority, usize second_priority>
        struct info<idx_association_storage_order_t<first_priority, second_priority>> {
            static constexpr bool is_idx_association_storage_order_v = true;
            using type = idx_association_storage_order_t<first_priority, second_priority>;
            
            static constexpr usize first_priority_order_v = type::first_priority_order_v;
            static constexpr usize second_priority_order_v = type::second_priority_order_v;
            
            static constexpr bool is_mono_ordering_v = type::is_mono_ordering_v;
            
            static constexpr index_association_storage_order value = index_association_storage_order(
                first_priority, second_priority
            );
        };
    }
    
    
    template <usize a, usize b>
    using idx_association_storage_order_t = idx_association_storage::idx_association_storage_order_t<a, b>;
    

    namespace _detail {
        struct _sentinel {};
    }
    
    /**
     * Holds types for defining index systems and data stored with them.
     * 
     * Also allows you to store data with an association between indices.
     * 
     * Recommendation: struct-inherit and crtp this so that you don't accidentally intermingle data managers for
     * different datasets.
     * 
     * Indexed data systems increment the association it was generated from's lock count if the lock was not
     * nullptr, and decrement it when destroyed. This prevents modification of the association while it's
     * index arrangement is being used. 
     */
    template <usize N, typename Derived, typename AssociatedData = _detail::_sentinel>
    class idx_system_data_manager_t
    {
    public:
        static constexpr usize num_idx_systems = N;
        
        struct association_t {
            
            using idx_data_t = std::array <index_t, N>;
        private:
            // Don't let other classes screw with ordering by making this private.
            idx_data_t indices;
            
        public:
            AssociatedData ad;
            
            template <typename ADRef>
            association_t(idx_data_t idx_association, ADRef&& ad);
            association_t(idx_data_t idx_association);
            association_t();
            
            const idx_data_t& get_idxs() const {return this->indices;}
            const index_t& idx(const index_t idx_system_v) const;
            
            AssociatedData& assoc_data();
            const AssociatedData& assoc_data() const;
            const AssociatedData& const_assoc_data() const;
            
        };
        
        
        using raw_association_data_t = std::vector<association_t>;
        struct association_state_lock_t; // predecl
        
        template <typename idx_sort_order> class association_data_accessor_t; // Predecl for friend class template.
        /**
         * Holds the raw association data. Is only move-constructible. Contains a lock to prevent being changed
         * when it's indicated index arrangement is being used.
         */
        struct association_data_t {
            using usage_counter_t = std::atomic<u64>;
        protected:
            // Actual association data. Gets shuffled around a bit.
            // We're gonna put this in a unique_ptr to avoid some nasty breakage.
            std::unique_ptr<raw_association_data_t> data;
            // How many indexed_data_ts this arrangement of index associations is being used by.
            std::unique_ptr<usage_counter_t> usage_count;
            // Stop and start using this association data.
            void start_using() {
                *usage_count += 1;
            }
            void stop_using() {
                *usage_count -= 1;
            }
        public:
            association_data_t() = delete;
            // Remove the copy constructor so we don't accidentally do that.
            association_data_t(const association_data_t& other) = delete;
            association_data_t(association_data_t&& other) : 
                data(std::move(other.data)), 
                usage_count(std::move(other.usage_count))
                {}
            // Create from raw association data.
            association_data_t(raw_association_data_t&& data_raw) : data(std::make_unique<raw_association_data_t>(std::move(data_raw))) {
                usage_count = std::make_unique<usage_counter_t>();
                *usage_count = 0;
            }
            
            association_data_t& operator=(const association_data_t& other) = delete;
            association_data_t& operator=(association_data_t other) = delete;
            
            auto begin() {
                return data->begin();
            }
            
            auto begin() const {
                return data->cbegin();
            }
            
            auto cbegin() const {
                return data->cbegin();
            }
            
            auto end() {
                return data->end();
            }
            
            auto end() const {
                return data->cend();
            }
            
            auto cend() const {
                return data->cend();
            }
            
            auto resize(usize s) {
                return data->resize(s);
            }
            
            auto size() const {
                return data->size();
            }
            
            // Note lack of length changes 
            
            auto operator[] (usize idx) {
                return (*data)[idx];
            }
            
            auto operator[] (usize idx) const {
                return (*data)[idx];
            }
            
            /**
             * Create a deep copy of this association.
             * 
             * We create this method because we must be explicit about it. Unintentional breaking of locks
             * would occur if implicit or constructive copy was allowed.
             */
            association_data_t clone() const {
                raw_association_data_t new_data(data->begin(), data->end());
                return association_data_t(std::move(new_data));
            }
            
            
            // For incrementing/decrementing the count.
            friend struct association_state_lock_t;
            
            // For accessing raw data.
            template <typename> friend class association_data_accessor_t;
            
            template <typename OStream>
            inline friend std::ostream& operator<<(OStream& o, const association_data_t& data) {
                o << "<== ASSOCIATION-DATA (USAGE COUNT: " << *(data.usage_count) << ") ==>" << "\n";
                util::printvec(o, *(data.data));
                o << "\n<== END ASSOCIATION-DATA ==>";
                return o;
            }
            
        };
    
        /**
         * Holds a lock on the data arrangement for the given association data. 
         * 
         * Note that this means if all accessors to the association data get deleted, it still hangs
         * around until all the indexed data is gone too.
         * 
         * If it's nullptr then we do nothing.
         * 
         * This automatically starts using on creation and stops using on destruction.
         */
        struct association_state_lock_t {
            using ptr_t = std::shared_ptr<association_data_t>;
            ptr_t association_data;
            
            association_state_lock_t(const association_state_lock_t& other) :
                association_state_lock_t(other.association_data)
                {}
                
            // mov constructor
            association_state_lock_t(association_state_lock_t&& other) : //mov so no use/unuse counts are broken
                association_data(std::move(other.association_data)) 
                {}
                
            association_state_lock_t (ptr_t a) {
                association_data = a;
                if (association_data) {
                    association_data->start_using();
                }
                
            }
            
            ~association_state_lock_t() {
                if (association_data) {
                    association_data->stop_using();
                }
            }
            
            /**
             * Checks if this is a statelock for the provided association data.
             */
            bool locks(const association_data_t& d) const {
                if (!association_data) return false; // nullptr.
                return std::addressof(*association_data) == std::addressof(d);
            }
            
            /**
             * Throws a std::range_error if this lock does not lock the given association data state.
             * 
             * (since the association data indicates a complex series of interlocking ranges.
             */
            void require_locks(const association_data_t& d) const {
                if (!locks(d)) throw std::range_error("Required association state locking was not done");
            }
        };
        
        template <usize idx_system, typename idx_data_storage_system, typename T, typename... _Rest>
        class indexed_data_t;
        
        
        /**
         * Provides a standard interface for calling functions on an element of a chunk of indexed data.
         * 
         * If the index data is fixed-width, the processor function should take an index argument and 
         * an argument for each element in the fixed width data. You can take references if you want,
         * there is no copy if you want to modify data.
         * 
         * If the index data is dynamic width, the processor function should take an index argument and
         * the start and and end iterators for the data.
         */
        template <
            typename idx_data_storage_system,
            usize idx_system_idx,
            typename T,
            typename... _Rest,
            typename ProcessorFunction
        > static auto call_on_element(
            ProcessorFunction&& processor, 
            indexed_data_t<idx_system_idx, idx_data_storage_system, T, _Rest...>& indexed_data,
            usize idx
        ) -> auto {
            using idx_data_t = indexed_data_t<idx_system_idx, idx_data_storage_system, T, _Rest...>;
            
            using idx_data_storage_system_info_t = data_storage_system::info<idx_data_storage_system>;
            
            typename idx_data_t::raw_data_iterator_t start_idx_iter;
            typename idx_data_t::raw_data_iterator_t end_idx_iter;
            std::tie(start_idx_iter, end_idx_iter) = indexed_data.get_for_index(idx);
            if constexpr(idx_data_storage_system_info_t::value.is_dynamic()) {
                return processor(idx, start_idx_iter, end_idx_iter);
            } else {
                return util::call_with_n_next_elements<idx_data_storage_system_info_t::elements_per_idx_v>::call(
                    start_idx_iter,
                    std::forward<ProcessorFunction>(processor),
                    idx
                );
            }
        }
        
        template <
            typename idx_data_storage_system,
            usize idx_system_idx,
            typename T,
            typename... _Rest,
            typename ProcessorFunction
        > static auto call_on_element(
            ProcessorFunction&& processor, 
            const indexed_data_t<idx_system_idx, idx_data_storage_system, T, _Rest...>& indexed_data,
            usize idx
        ) -> auto {
            using idx_data_t = indexed_data_t<idx_system_idx, idx_data_storage_system, T, _Rest...>;
            
            using idx_data_storage_system_info_t = data_storage_system::info<idx_data_storage_system>;
            
            typename idx_data_t::const_raw_data_iterator_t start_idx_iter;
            typename idx_data_t::const_raw_data_iterator_t end_idx_iter;
            std::tie(start_idx_iter, end_idx_iter) = indexed_data.get_for_index(idx);
            if constexpr(idx_data_storage_system_info_t::value.is_dynamic()) {
                return std::invoke(std::forward<ProcessorFunction>(processor), idx, start_idx_iter, end_idx_iter);
            } else {
                return util::call_with_n_next_elements<idx_data_storage_system_info_t::elements_per_idx_v>::call(
                    start_idx_iter,
                    std::forward<ProcessorFunction>(processor),
                    idx
                );
            }
        }
        
        
        /**
         * Call a processor function that performs an operation from data associated with a pair of
         * indices in two different indexed data systems, the "from" data+system and "to" data+system.
         * 
         * Note that this cannot deal with changing the width of a data chunk in a dynamic width output.
         * 
         * The first argument is the actual processor function.
         * 
         * -----
         * 
         * If the "from" data is fixed/unique width data, the processor function should take a single index parameter   
         * and a parameter list of the fixed length of that type.
         * 
         * If the "from" data is dynamic width data, the processor function should take an index parameter, and 
         * start and end const iterator to the data to be processed from.
         * 
         * -----
         * 
         * If "to" data is dynamic width data, the function should return a callable that takes in an index parameter,
         * and a start and end iterator to the data to fill up with stuff.
         * 
         * If "to" data is fixed/unique width data it should return a callable that takes an index parameter,
         * and a parameter list of references to the data type of this indexed data.
         * 
         * -----
         * 
         * Note that the output of the processor function is called immediately after the input.
         * 
         * Parameter @param from_idx is the index to the data to process
         * 
         * Parameter @param from_idx_data is the data to process.
         * 
         * Parameter @param to_idx is the index to where we should put the result of processing in the output idx data
         * 
         * Parameter @param to_idx_data is the data to write the result of processing to.
         */
        template <
            typename from_system, 
            typename to_system, 
            usize _start_idx_system,
            usize _end_idx_system,
            typename _start_T,
            typename _end_T,
            typename... _start_Rest,
            typename... _end_Rest,
            typename ProcessorFunction
        >
        static void call_element_processor(
            ProcessorFunction&& processor, 
            usize from_idx, 
            const indexed_data_t<_start_idx_system, from_system, _start_T, _start_Rest...>& from_idx_data,
            usize to_idx,
            indexed_data_t<_end_idx_system, to_system, _end_T, _end_Rest...>& to_idx_data
        ) {
            // Get the callable to process the output from the input.
            auto processor_result = call_on_element(
                std::forward<ProcessorFunction>(processor),
                from_idx_data,
                from_idx
            );
            // Run it on the output data.
            call_on_element(
                processor_result,
                to_idx_data,
                to_idx
            );
        }
        
        /**
         * Type - tagged vector to hold data of a given index system and index-data association type.
         */
        template <usize idx_system, typename idx_data_storage_system, typename T, typename... _Rest>
        class indexed_data_t {
        public:
            static_assert(
                data_storage_system::info<idx_data_storage_system>::is_data_width_type_v, 
                "idx_system_type must be a tag type"
            );
            static_assert(
                idx_system < N,
                "idx system for the indexed data is too large for the containing index data manager."
            );
            
            static constexpr usize idx_system_idx = idx_system;
            static constexpr idx_data_storage_system_t idx_data_storage_system_v 
                = data_storage_system::info<idx_data_storage_system>::value;
            
            /**
             * Only makes sense for non-dynamic sized arrays.
             */
            static constexpr usize this_elem_per_idx = idx_data_storage_system_v.get_element_count();
            
            using data_t = std::vector <T, _Rest...>;
            using data_element_t = T;
            
            /**
             * Holds the index into the raw data for the start of the data for that index.
             * 
             * This is only used in dynamic index systems because the other ones have an efficient
             * calculation.
             */
            using delta_index_t = std::vector <index_t, _Rest...>;
            
        private:
            data_t raw_data;
            delta_index_t delta_index_data;
            index_t length; // Holds the number of data sets that can be indexed.
            // Lock on the arrangement for this index data.
            association_state_lock_t association_state_lock;
            
        public:
            using raw_data_iterator_t = typename data_t::iterator;
            using raw_data_const_iterator_t = typename data_t::const_iterator;
            
        protected:
            /**
             * Moves the raw data into safer indexed data mangement.
             * 
             * This assumes the raw data is in the index system supposed by this thing.
             * 
             * If the indexed data is not a multiple of the index system size, the last little chunk of data
             * is ignored.
             * 
             * Does not work for dynamic systems. Use the other constructor.
             * 
             * Protect the constructors and constructor methods.
             */
            indexed_data_t(data_t&& raw_data, association_state_lock_t state_lock) : 
                raw_data(std::move(raw_data)), 
                delta_index_data(),
                length(), 
                association_state_lock(state_lock){
                static_assert(
                    !idx_data_storage_system_v.is_dynamic(), 
                    "Constructor only works on non-dynamic systems"
                );
                length = this->raw_data.size()/idx_data_storage_system_v.get_element_count();
            }
            
            indexed_data_t(data_t&& raw_data, delta_index_t&& delta_data, association_state_lock_t state_lock):
                raw_data(std::move(raw_data)), 
                delta_index_data(std::move(delta_data)),
                length(this->delta_index_data.size()), 
                association_state_lock(state_lock){
                static_assert(
                    data_storage_system::info<idx_data_storage_system>::value.is_dynamic(), 
                              "delta_data is only required for dynamic sized data."
                );
            }
            
        public:
            /**
             * Get the offset for the start of the data for idx.
             * 
             * Undefined (probably segfault) if idx out of range.
             */
            index_t get_start_offset_for(index_t idx) const {
                if constexpr (idx_data_storage_system_v.is_dynamic()) {
                    return delta_index_data[idx];
                } else {
                    return idx * idx_data_storage_system_v.get_element_count();
                }
            }
            
            /**
             * Gets whether this indexed data locks the state of the given association data.
             */
            bool locks_association_state(const association_data_t& d) const {
                return association_state_lock.locks(d);
            }
            
            /**
             * Requires that this indexed data locks the state of the given association data,
             * throwing a std::range_error if it does not.
             */
            void require_locks_association_state(const association_data_t& d) const {
                association_state_lock.require_locks(d);
            }

            /**
             * Get the iterator for a given offset into the raw data.
             */
            raw_data_iterator_t get_data_iterator_for_offset(index_t offset) {
                return (offset < raw_data.size()) ? raw_data.begin() + offset : raw_data.end();
            }
            
            raw_data_const_iterator_t get_data_iterator_for_offset(index_t offset) const {
                return (offset < raw_data.size()) ? raw_data.cbegin() + offset : raw_data.cend();
            }
            
            /**
             * Get the offset for the end of the data for idx. Can be one beyond the end of the
             * data object.
             */
            index_t get_end_offset_for(index_t idx) const {
                if constexpr(idx_data_storage_system_v.is_dynamic()) {
                    if(idx + 1 == this->length) {
                        // The end of the data for the last object is simply the end of the vector.
                        return raw_data.size();
                    }
                    return delta_index_data[idx + 1];
                } else {
                    // This cleverly splices off any data that is not a multiple of the number of elements
                    // per idx at the end.
                    return (idx + 1) * idx_data_storage_system_v.get_element_count();
                }
            }
            

            
            /**
             * Get iterators pointing to the start and end of the given index data in the data.
             */
            std::pair<raw_data_iterator_t, raw_data_iterator_t> get_for_index(index_t idx) {
                raw_data_iterator_t data_start = get_data_iterator_for_offset(get_start_offset_for(idx));
                raw_data_iterator_t data_end = get_data_iterator_for_offset(get_end_offset_for(idx));
                return {data_start, data_end};
            }
            
            std::pair<raw_data_const_iterator_t, raw_data_const_iterator_t> get_for_index(index_t idx) const {
                raw_data_const_iterator_t data_start = get_data_iterator_for_offset(get_start_offset_for(idx));
                raw_data_const_iterator_t data_end = get_data_iterator_for_offset(get_end_offset_for(idx));
                return {data_start, data_end};
            }
            
            /**
             * Get the number of elements for a given index. Undefined for indices out of range.
             */
            usize number_of_elements_for(index_t idx) const {
                // Non-dynamic systems just return a constant
                if constexpr(!idx_data_storage_system_v.is_dynamic()) {
                    return idx_data_storage_system_v.get_element_count();
                } else {
                    raw_data_const_iterator_t s;
                    raw_data_const_iterator_t e;
                    std::tie(s, e) = get_for_index(idx);
                    return e - s;
                }
            }
            
            /**
             * Get ranges of indices into the data set such that the number of raw data elements
             * in each range is roughly equal. Ranges are standard [)
             * 
             * For dynamic-sized systems this is quite expensive but for static sized ones it isn't.
             */
            std::vector <std::pair<index_t, index_t>> splice_roughly_equally_into(const usize count) {
                const index_t raw_data_length = raw_data.size();
                
                // The rough delta *in the raw data* that should exist in the ranges.
                // This is what the average delta should come close to to evenly distribute over count.
                // This is floored so current_count * this is always less than the raw data length.
                const usize raw_data_segment_rough_delta = raw_data_length/count; 
                
                std::vector <std::pair<index_t, index_t>> result;
                result.resize(count);
                
                // Special case for very low data lengths, where we divide into the minimum of 
                // (length, raw_data_length) chunks and fill the rest with the end.
                // This avoids division issues with raw_data_segment_rough_delta being 0
                if (raw_data_length < count or length < count) {
                    const usize divvy_up_into = std::min(length, raw_data_length);
                    auto sub_result = this->splice_roughly_equally_into(divvy_up_into);
                    std::copy(sub_result.begin(), sub_result.end(), result.begin());
                    // Fill up all the remaining ones with indices to end.
                    std::fill(result.begin() + divvy_up_into, // The-one-after the end of sub_result
                              result.end(), 
                              std::pair <index_t, index_t>{length, length}
                    );
                    return result;
                }
                
                
                // Find the minimum index for each part of the count.
                // This is the minimum index for the current count - holds the index where we get to the number
                // of raw data elements required to switch to the current count range.
                usize indices_below_current_count = 0;
                
                // Holds the total raw data elements up to the current point.
                usize total_raw_data_elements_up_to_point = 0;
                for (u64 i = 1; i < count; i++) {
                    if constexpr (!idx_data_storage_system_v.is_dynamic()) {
                        constexpr usize element_count = idx_data_storage_system_v.get_element_count();
                        
                        // The number of raw elements we want for near-perfect splicing of the data.
                        total_raw_data_elements_up_to_point = i * raw_data_segment_rough_delta;
                        indices_below_current_count = total_raw_data_elements_up_to_point/element_count;
                    } else {
                        
                        // Dynamic accumulation.
                        // While we have insufficient elements for the current range, accumulate more.
                        while (total_raw_data_elements_up_to_point < i * raw_data_segment_rough_delta ) {
                            indices_below_current_count+=1;
                            total_raw_data_elements_up_to_point += number_of_elements_for(indices_below_current_count);
                        }
                    }
                    // Set the end point of the previous count range and the startpoint of the current count range.
                    result[i - 1].second = indices_below_current_count;
                    result[i].first = indices_below_current_count;
                }
                
                // Provide values for the start and end ranges.
                result[0].first = 0;
                result[result.size() - 1].second = this->length;
                
                return result;
            }
            
            /**
             * Regenerate partial contents of this from the partial contents of the other.
             * 
             * If the other is a unique type, the processor function should take an index parameter 
             * and a single parameter which is an instance of the data type
             * 
             * If the other is a fixed type, the processor function should take a single index parameter and a
             * parameter list of the fixed length of that type.
             * 
             * If the other is a dynamic type, the processor function should take an index parameter, and 
             * start and end iterator to the data.
             * 
             * -----
             * 
             * If this is a dynamic type, the function should return a callable that takes in an index parameter,
             * and a start and end iterator to the data to fill up with stuff.
             * 
             * If this is a unique type it should return a callable that takes an index parameter,
             * and reference to a single item of the data type of this indexed data.
             * 
             * If this is a fixed width type it should return a callable that takes an index parameter,
             * and a parameter list of references to the data type of this indexed data.
             * 
             * -----
             * 
             * Functionally this means unique and fixed width types have identical requirements (fixed types
             * are just more generalised).
             */
            template <
                typename ProcessorFunction,
                typename  other_idx_system_type,
                typename Q, typename... _OtherRest
            > void regenerate_from_other_indexed_data(
                const indexed_data_t<idx_system_idx, other_idx_system_type, Q, _OtherRest...>& other,
                ProcessorFunction&& processor,
                const std::pair <index_t, index_t> data_range
            ) {
                util::do_range_indexed(
                    data_range.first, 
                    data_range.second, 
                    [&processor, &other, this](u64 idx){
                        call_element_processor(
                            std::forward <ProcessorFunction>(processor), idx, other, idx, *this
                        );
                    }
                );
            }
            
            // This one isn't protected because it can duplicate an association state lock from the index data it
            // is created from 
            
            /**
             * Creates a new indexed data chunk from the passed one.
             * 
             * The initialiser function is similar to that used in regenerate_from_other_indexed_data.
             * 
             * --------
             * 
             * If the created-from indexed data is a unique or fixed-size type, then the initialiser function
             * will be passed (idx, current_data... (param list)) and should return a callable.
             * 
             * If the created-from indexed data is a dynamic sized type, then the initialiser function
             * will be passed (idx, raw data start iterator, raw data end iterator) and should return a callable.
             * 
             * ---------
             *
             * If the to-be-created indexed data is a fixed-size type, then the returned callable should take
             * an idx argument and a list of mutable references to the raw data in the new object.
             * 
             * If the to-be-created indexed data is a dynamic type, then the returned callable should take
             * an idx argument and a mutuable reference to an empty std::vector of raw data for the new data.
             * 
             * ---------
             * 
             * To enable better performance you can provide an estimate (or over-estimate) of the number of elements
             * per index for dynamic index system outputs. The default value of 0 just causes the vector to preallocate
             * one raw data element in the output for each in the input.
             */
            template <
                typename InitialiserFunction, 
                typename other_idx_system_type, 
                typename Q, typename... _OtherRest
            >
            static indexed_data_t create_from(
                const indexed_data_t<idx_system_idx, other_idx_system_type, Q, _OtherRest...>& other,
                InitialiserFunction&& initialiser,
                const usize estimated_elements_per_index = 0
            ) {
                //using other_t = indexed_data_t<idx_system_idx, other_idx_system_type, Q, _OtherRest...>;
                data_t new_raw_data;
                static constexpr bool dyn = idx_data_storage_system_v.is_dynamic();
                
                // Dynamic width uses vector of type as result, static width uses array of values.
                // of the appropriate width to fill one index
                using result_per_idx_t = std::conditional_t<
                    dyn, std::vector<T>, std::array<T, this_elem_per_idx>
                >; 
                    
                // Only used if the output indexed_data_t is dynamic
                delta_index_t new_delta_data;
                
                if constexpr(dyn) {
                    new_delta_data.reserve(other.length);
                    // Preallocate based on estimated element count. If none provided, we just guess 1 per element.
                    if (estimated_elements_per_index == 0) {
                        new_raw_data.reserve(other.raw_data.size());
                    } else {
                        new_raw_data.reserve(estimated_elements_per_index * other.length);
                    }
                } else {
                    // Creating a non-dynamic set of index data.
                    new_raw_data.reserve(other.length * idx_data_storage_system_v.get_element_count());
                }
                
                crnn::util::do_n_indexed(other.length, [&](index_t idx){
                    result_per_idx_t result_for_this;
                    
                    // the length of the current raw data is the start index of the new data
                    // and hence what we need to append to new_delta_data
                    new_delta_data.push_back(new_raw_data.size());
                    
                    result_for_this = call_on_element(
                        std::forward<InitialiserFunction>(initialiser),
                        other, 
                        idx
                    );
                    new_raw_data.insert(new_raw_data.cend(), result_for_this.begin(), result_for_this.end());
                });
                
                
                if constexpr(dyn) {
                    return indexed_data_t(
                        std::move(new_raw_data), 
                        std::move(new_delta_data), 
                        other.association_state_lock
                    );
                } else {
                    return indexed_data_t(
                        std::move(new_raw_data),
                        other.association_state_lock
                    );
                }
            }
            
            
            // This IS protected because we need a state lock.
            /**
             * Generate the indexed data with a function of the index.
             * 
             * For fixed data-per-index datasets return an array of the appropriate length and type.
             * For dynamic data-per-index datasets return a std::vector <T> for the given index.
             * 
             * Allows you to provide an optional elements-per-index average for dynamic index systems
             * so memory can be preallocated appropriately. If not present then we allocate the provided
             * size in dynamic memory data.
             */
            
        protected:
            template <typename GeneratorFunction>
            static indexed_data_t generate(
                const usize size, 
                GeneratorFunction&& gen,
                association_state_lock_t state_lock,
                usize estimated_elements_per_index = 0
            ) {
                data_t new_raw_data;
                delta_index_t new_delta_data;
                
                // Preallocate memory
                if constexpr(idx_data_storage_system_v.is_dynamic()){
                    if (estimated_elements_per_index == 0) {
                        new_raw_data.reserve(size);
                    } else {
                        new_raw_data.reserve(size * estimated_elements_per_index);
                    }
                    new_delta_data.reserve(size);
                } else {
                    new_raw_data.reserve(this_elem_per_idx * size);
                }
                
                
                // Actually do generation.
                crnn::util::do_n_indexed(size, [&](index_t idx){
                    static constexpr bool dyn = idx_data_storage_system_v.is_dynamic();
                    using result_t = std::conditional_t<
                        dyn, std::vector<T>, std::array<T, this_elem_per_idx>
                    >; // Dynamic width uses vector of type as result, static width uses array of values.
                    // of the appropriate width to fill one index
                    
                    result_t result = gen(idx);                
                    if constexpr(dyn) {
                        new_delta_data.push_back(new_raw_data.size());
                    }
                    new_raw_data.insert(new_raw_data.cend(), result.begin(), result.end());
                });
                
                if constexpr(idx_data_storage_system_v.is_dynamic()) {
                    return indexed_data_t(std::move(new_raw_data), std::move(new_delta_data), state_lock);
                } else {
                    return indexed_data_t(std::move(new_raw_data), state_lock);
                }
            }
            
            // We want the association accessor to be a friendy b o i so it can create indexed_data_t's easily
            // with it's lock.
            template <typename idx_sort_order> friend class association_data_accessor_t;
            
        };
        
        /**
         * Define some data structures and functions for holding index orderings type-safely, to 
         * prevent using the wrong index system ordering by accident.
         */
    protected:
        /**
         * Sort a raw chunk of association data into the order specified by the index system sort order.
         * 
         * idx_sort_order is the ordering to use.
         */
        template <typename idx_sort_order>
        struct sort_raw_data_to {
            static_assert(
                idx_association_storage::info<idx_sort_order>::is_idx_association_storage_order_v,
                "The index sort order tag type must actually be a valid index association data order"
            );
            
            template <typename StartIter, typename EndIter>
            static void call(StartIter begin, EndIter end) {
                constexpr usize first_priority_idx_system = idx_association_storage::info<
                    idx_sort_order
                >::first_priority_order_v;
                
                constexpr usize second_priority_idx_system = idx_association_storage::info<
                    idx_sort_order
                >::second_priority_order_v;
                
                std::sort(
                    begin,
                    end,
                    [](const association_t& before, const association_t& after) -> bool {
                        
                        auto bef = std::tie(
                            before.idx(first_priority_idx_system), before.idx(second_priority_idx_system)
                        );
                        auto aft = std::tie(
                            after.idx(first_priority_idx_system), after.idx(second_priority_idx_system)
                        );
                        
                        return bef < aft; // Does lexicographical ordering
                    }
                );
            }
        };
            
    public:
        
        /**
         * Holds association data in a specific order in a vector.
         * Will automatically sort on construction and stuff like that to match the ordering
         * unless the supplied vector is also of the given idx sort order type.
         */
        template <typename idx_sort_order>
        class association_data_accessor_t
        {
            std::shared_ptr<association_data_t> assoc_data;
            // Precalculated start index for the association data of each element.
            // Same format as indexed_data_t's delta_data.
            std::vector <index_t> start_for_each_index;
        protected:
            association_state_lock_t create_state_lock() {
                return association_state_lock_t(assoc_data);
            }
            
        public:
            static_assert(
                idx_association_storage::info<idx_sort_order>::is_idx_association_storage_order_v,
                "The index data sort order must actually be a valid tag type."
            );
            
            // Get the order priorities for the given index systems.
            static constexpr usize pois = idx_association_storage::info<idx_sort_order>::first_priority_order_v;
            static constexpr usize spois = idx_association_storage::info<idx_sort_order>::second_priority_order_v;
            
            
            // Some simple public association constructors from same associations and different ones.
            template <typename other_idx_sort_order>
            association_data_accessor_t(
                    association_data_accessor_t<other_idx_sort_order>&& other
            ) : 
            assoc_data(std::move(other.assoc_data)), 
            start_for_each_index(std::move(other.start_for_each_index)) {
                // Sort data in place if the thing is not in the same order as this one.
                if constexpr(!std::is_same_v<idx_sort_order, other_idx_sort_order>) {
                    sort_raw_data_to<idx_sort_order>::call(this->assoc_data->begin(), this->assoc_data->end());
                    this->start_for_each_index = util::locations_of_change(
                        this->assoc_data->begin(), 
                        this->assoc_data->end(), 
                        [](const association_t& data) {
                            return data.idx(pois);
                        }
                    );
                }
            }
            
            template <typename other_idx_sort_order>
            association_data_accessor_t(const association_data_accessor_t<other_idx_sort_order>&) = delete;
            
            template <typename other_idx_sort_order>
            association_data_accessor_t(association_data_accessor_t<other_idx_sort_order>) = delete;
            
            // Must explicitly move any old data into this one.
            // also auto-sorts
            association_data_accessor_t(association_data_t&& data) : 
                assoc_data(std::make_shared<association_data_t>(std::move(data))) {
                auto dstart = this->assoc_data->begin();
                auto dend = this->assoc_data->end();
                sort_raw_data_to<idx_sort_order>::call(dstart, dend);
                this->start_for_each_index = util::locations_of_change(
                    this->assoc_data->begin(), 
                    this->assoc_data->end(), 
                    [](const association_t& data) {
                        return data.idx(pois);
                    }
                );
            }
            
            /**
             * Deep copy the current data into a new association of a different order.
             */
            template <typename new_idx_sort_order>
            auto clone_as() -> association_data_accessor_t <new_idx_sort_order> {
                auto cloned_association_data = assoc_data->clone();
                return association_data_accessor_t<new_idx_sort_order>(
                    std::move(cloned_association_data)
                );
            }
            
            /**
             * Get the maximum index in the primary ordering for this association accessor
             * 
             */
            usize get_total_primary_ordered_indices() const {
                // If we have any associations, get the last one, since they are sorted in order
                // of the primary ordering this will have the maximum index. Add one to get a count.
                
                // Else we have no indices.
                const std::optional last_element = 
                    (assoc_data->size() == 0) ? 
                        std::optional<association_t>(std::nullopt) : 
                        std::optional<association_t>((*assoc_data)[assoc_data->size() - 1]);
                        
                const usize total_idxs = last_element.has_value() ? 
                    last_element.value().idx(pois) + 1: 0;
                return total_idxs;
            }
            
            /**
             * Get the average number of associations/elements per primary-ordering index, 
             * ceilinged (3.4/idx => 4/idx returned). If we have no elements we give an estimation of zero.
             */
            usize estimated_associations_per_primary_index() const {
                usizec total_idxs = this->get_total_primary_ordered_indices();
                if (total_idxs == 0) return 0;
                f64c estimated_associations_per_idx = std::ceil(
                    static_cast<f64>(this->assoc_data->size())/static_cast<f64>(total_idxs)
                );
                
                return static_cast<usize>(estimated_associations_per_idx);
                
            }
            
        protected:
            /**
             * Call a function on all associations for an index in the primary indexing system of this accessor.
             * 
             * If the system also has a second priority ordering, the data in the iterators will be in that order.
             * 
             * The processor function should take an index argument and a start and end iterator
             * to a range of association_t.
             * 
             */
            template <
                typename ProcessorFunction
            >
            auto call_on_associations_for_idx(
                ProcessorFunction&& func,                
                usize idx_in_primary_ordering
            ) {
                const std::pair assocs_for_idx = util::get_nth_data_block(
                    assoc_data->begin(), //In-place modify of data.
                    assoc_data->end(),
                    start_for_each_index,
                    idx_in_primary_ordering
                );
                auto result = std::invoke(
                    std::forward<ProcessorFunction>(func), 
                    idx_in_primary_ordering,
                    assocs_for_idx.first,
                    assocs_for_idx.second
                );
                return result;
            }
            
            
            template <
                typename ProcessorFunction
            >
            auto call_on_associations_for_idx(
                ProcessorFunction&& func,
                usize idx_in_primary_ordering
            ) const {
                const std::pair assocs_for_idx = util::get_nth_data_block(
                    assoc_data->cbegin(), // No modify allowed.
                    assoc_data->cend(),
                    start_for_each_index,
                    idx_in_primary_ordering
                );
                auto result = std::invoke(
                    std::forward<ProcessorFunction>(func), 
                    idx_in_primary_ordering,
                    assocs_for_idx.first,
                    assocs_for_idx.second
                );
                return result;
            }
        
        protected:
            
            
            /**
             * Process the associations and data (from a given indexed data object) for a given index
             * together.
             * 
             * This requires that the indexed data has the same index system as the primary ordering
             * in this association data accessor, and that the indexed data locks the state of the
             * association data.
             * 
             * By default the locking is checked. You can pass true as a final argument to prevent this check
             * (this is only for performance reasons if you already checked once).
             * 
             * @param processor is an invokable which has behaviour. Firstly, it takes an index argument, 
             * and a start and end iterator to association_t&. Then it should return an invokable that
             * takes varying arguments depending on the type of index_data - all index data types take
             * an index argument first, then the next arguments are either a start and end iterator to
             * the data for that index (dynamic-width index_data_t), or an argument list of references
             * to each element in the fixed-width data.
             * 
             * The resulting invokable is called immediately.
             * 
             */
            template <
                typename ProcessorFunction,
                typename other_idx_system_type, 
                typename Q, 
                typename... _OtherRest
            >
            auto process_associations_and_data_for_index(
                indexed_data_t<pois, other_idx_system_type, Q, _OtherRest...>& data,
                ProcessorFunction&& processor,
                index_t idx,
                const bool assume_statelocked = false
            ) {
                if(!assume_statelocked) data.require_locks_association_state(*assoc_data);
                
                auto to_invoke = call_on_associations_for_idx(std::forward<ProcessorFunction>(processor), idx);
                return call_on_element(std::move(to_invoke), data, idx);
            }
            
            // BEGIN const-variation implementations.
            
            // This monstrocity is what happens when you have to redefine functions for const correctness
            // >:(
            // I am so sorry for future me (or anyone else) who has to maintain these. Please try and keep
            // all the definitions together at least.
            template <
                typename ProcessorFunction,
                typename other_idx_system_type, 
                typename Q, 
                typename... _OtherRest
            >
            auto process_associations_and_data_for_index(
                indexed_data_t<pois, other_idx_system_type, Q, _OtherRest...>& data,
                ProcessorFunction&& processor,
                index_t idx,
                const bool assume_statelocked = false
            ) const {
                if(!assume_statelocked) data.require_locks_association_state(*assoc_data);
                
                auto to_invoke = call_on_associations_for_idx(std::forward<ProcessorFunction>(processor), idx);
                return call_on_element(std::move(to_invoke), data, idx);
            }
            
            template <
                typename ProcessorFunction,
                typename other_idx_system_type, 
                typename Q, 
                typename... _OtherRest
            >
            auto process_associations_and_data_for_index(
                const indexed_data_t<pois, other_idx_system_type, Q, _OtherRest...>& data,
                ProcessorFunction&& processor,
                index_t idx,
                const bool assume_statelocked = false
            ) {
                if(!assume_statelocked) data.require_locks_association_state(*assoc_data);
                
                auto to_invoke = call_on_associations_for_idx(std::forward<ProcessorFunction>(processor), idx);
                return call_on_element(std::move(to_invoke), data, idx);
            }
            
            
            template <
                typename ProcessorFunction,
                typename other_idx_system_type, 
                typename Q, 
                typename... _OtherRest
            >
            auto process_associations_and_data_for_index(
                const indexed_data_t<pois, other_idx_system_type, Q, _OtherRest...>& data,
                ProcessorFunction&& processor,
                index_t idx,
                const bool assume_statelocked = false
            ) const {
                if(!assume_statelocked) data.require_locks_association_state(*assoc_data);
                
                auto to_invoke = call_on_associations_for_idx(std::forward<ProcessorFunction>(processor), idx);
                return call_on_element(std::move(to_invoke), data, idx);
            }
            
            // END
            
        public:
            

            /**
             * Convert the associations in the accessor into a dynamic-width indexed_data_t of the
             * primary index system of this association accessor. The data for each index is
             * the result of calling the processor function with a const association_t& argument
             * and collecting all those.
             * 
             * Returning void is UB.
             */
            template <typename Processor>
            auto to_association_calculated_indexed_data(Processor&& processor) -> auto {
                using result_data_t = std::invoke_result_t<decltype(processor), const association_t&>;
                using idx_data_t = indexed_data_t<
                    pois,
                    data_storage_system::dynamic_width_data_t,
                    result_data_t
                >;
                
                usizec total_primary_idxs = this->get_total_primary_ordered_indices();
                usizec estimated_elements_per_index = this->estimated_associations_per_primary_index();
                
                return idx_data_t::generate(
                    total_primary_idxs, 
                    [&processor, this](const index_t primary_idx) -> std::vector <result_data_t> {
                         std::vector <result_data_t> result_for_this_idx = this->call_on_associations_for_idx(
                            [&processor](usize, auto start_iter, auto end_iter) { 
                                std::vector <result_data_t> res;
                                
                                auto cnt = std::distance(start_iter, end_iter);
                                
                                res.reserve(cnt);
                                
                                //Transform each association
                                std::for_each(start_iter, end_iter, [&](const association_t& a){
                                    res.push_back(std::invoke(processor, a));
                                });
                                return res;
                            },
                            primary_idx
                        );
                        return result_for_this_idx;
                    },
                    create_state_lock(),
                    estimated_elements_per_index
                );
                
            }
            
            /**
             * Create an indexed data mapping that when indexed via the primary
             * index in idx_sort_order gives data pointing to the associated secondary index
             * specified in the idx_sort_order, in index order for that primary index.
             */
            auto to_index_data() -> auto {
                return this->to_association_calculated_indexed_data([](const association_t& assoc) -> index_t{
                    return assoc.idx(spois);
                });
            }
            
            template <typename OStream>
            friend std::ostream& operator<<(OStream& o, const association_data_accessor_t& accessor) {
                o << "<== ADACCESSOR ~ Ordering[prim:" << pois << "|sec:" << spois << "]" << " ==>\n";
                o << *(accessor.assoc_data) << "\n";
                o << "<== END ADACCESSOR ==>";
                return o;
            } 
        };
        
        template <typename OStream> // So no errors happen when main class is instantiated but the AssociatedData is not printable.
        friend std::ostream& operator<<(OStream& o, const association_t& a) {
            o << "{[";
            // All before last if viable.
            util::do_n_indexed(N, [&](u64 idx){
                o << a.idx(idx) << (idx == N - 1 ? "" : ", "); // At the end, no comma.
            });
            // End
            o << "]";
            if constexpr (!std::is_same_v<AssociatedData, _detail::_sentinel>) {
                o << ":" << a.ad;
            }
            
            o << "}";
        
            return o;
        }
    };
    
    
    
    
    template <usize N, typename Derived, typename AssociatedData>
    template <typename ADRef>
    idx_system_data_manager_t<N, Derived, AssociatedData>::association_t::association_t::association_t(
        idx_data_t idx_association, 
        ADRef&& ad
    ) : indices(idx_association), 
        ad(std::forward<ADRef>(ad)) {
            // Only allowed to use this constructor if ad is not a nullopt_t (i.e. no type data associated).
        static_assert(
            !std::is_same_v<AssociatedData, _detail::_sentinel>, 
            "Cannot initialise non-type-associated association with a type"
        );
    }
    
    template <usize N, typename Derived, typename AssociatedData>
    idx_system_data_manager_t<N, Derived, AssociatedData>::association_t::association_t(idx_data_t idx_association) : 
        indices(idx_association), 
        ad(util::default_construct_t<AssociatedData>()) 
        {}
        
    template <usize N, typename Derived, typename AssociatedData>
    idx_system_data_manager_t<N, Derived, AssociatedData>::association_t::association_t() : 
        indices(), 
        ad(util::default_construct_t<AssociatedData>()) 
        {}
    
    template <usize N, typename Derived, typename AssociatedData>
    const index_t& 
    idx_system_data_manager_t<N, Derived, AssociatedData>::association_t::idx(const index_t idx_system_v) const {
        return indices[idx_system_v];
    }
    
    
    template <usize N, typename Derived, typename AssociatedData>
    AssociatedData& idx_system_data_manager_t<N, Derived, AssociatedData>::association_t::assoc_data() {
        return ad;
    }
    
    template <usize N, typename Derived, typename AssociatedData>
    const AssociatedData& idx_system_data_manager_t<N, Derived, AssociatedData>::association_t::assoc_data() const {
        return ad;
    }
    
    template <usize N, typename Derived, typename AssociatedData>
    const AssociatedData& idx_system_data_manager_t<N, Derived, AssociatedData>::association_t::const_assoc_data() const {
        return ad;
    }
    
    // Get whether a type is a CRTP-Derived type of idx_system_data_manager_t
    template <typename Derived>
    struct is_idx_system_data_manager {
        static constexpr bool value = false;
    };
    
    template <usize N, typename Derived, typename AD>
    struct is_idx_system_data_manager<idx_system_data_manager_t<N, Derived, AD>> {
        static constexpr bool value = true;
    };
    
    template <typename T>
    constexpr bool is_idx_system_data_manager_v = is_idx_system_data_manager<T>::value;
}


