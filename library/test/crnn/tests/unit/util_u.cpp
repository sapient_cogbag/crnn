/**
Copyright 2019 sapient_cogbag <sapient_cogbag@protonmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include <catch2/catch.hpp>
#include <crnn/persistence.hpp>
#include <crnn/util.hpp>
#include <crnn-test/crnn-test.hpp>
#include <random>
#include <sstream>
#include <map>

using namespace crnn::microtypedefs;

TEST_CASE("Testing range/index functions", "[crnn][crnn::util]") {
    SECTION("Sequentiality") {
        u64 i[3] = {0, 0, 0};
        u64 v_to_set = 0;
        SECTION("do_n_indexed") {
            crnn::util::do_n_indexed(3, [&v_to_set, &i](u64c idx){
                i[idx] = v_to_set;
                v_to_set++;
            });
            REQUIRE(i[0] == 0);
            REQUIRE(i[1] == 1);
            REQUIRE(i[2] == 2);
        }
        SECTION("do_range_indexed") {
            crnn::util::do_range_indexed(0, 3, [&v_to_set, &i](u64c idx) {
                i[idx] = v_to_set;
                v_to_set++;
            });
            REQUIRE(i[0] == 0);
            REQUIRE(i[1] == 1);
            REQUIRE(i[2] == 2);
        }
    }
    
    SECTION("Range functions do nothing on smaller or equal end") {
        bool did = false;
        crnn::util::do_range(1, 0, [&](){
            did = true;
        });
        REQUIRE_FALSE(did);
        
        crnn::util::do_range(1, 1, [&](){
            did = true;
        });
        REQUIRE_FALSE(did);
        
        crnn::util::do_range(0, 1, [&](){
            did = true;
        });
        REQUIRE(did);
    }
    
}

TEST_CASE("Locating sections of identical-processor-function-result in iterable", "[crnn][crnn::util]") {
    SECTION("Location generation") {
        SECTION("No processor function, only identity") {
            std::vector <u64> v = {1, 1, 1, 0, 0, 1, 2, 2};
            auto result = crnn::util::locations_of_change(v.begin(), v.end());
            REQUIRE(result == std::vector <usize>{0, 3, 5, 6});
        }
        
        
        SECTION("Getting a member") {
            using T = std::pair<u64, u64>;
            std::vector <T> v = {
                {0, 0},
                {0, 1},
                {1, 1},
                {2, 1},
                {2, 2},
                {0, 2},
                {0, 3},
                {0, 3}
            };
            auto result = crnn::util::locations_of_change(v.begin(), v.end(), &T::first);
            REQUIRE(result == std::vector<usize>{0, 2, 3, 5});
        }
        
        SECTION("Function") {
            using T = std::array<i64, 3>;
            std::vector<T> v = {
                {0, 0, 0}, // 0
                {0, -1, 1}, // 0
                {1, 0, 0}, // 1
                {0, -1, 2}, // 1
                {-3, 2, 2}, // 1
                {-1, -1, -1}, // -3
                {-2, -2, -2} // -6
            };
            
            // Find where the sums change.
            auto result = crnn::util::locations_of_change(v.begin(), v.end(), [](const T& val) -> i64 {
                return std::accumulate(val.begin(), val.end(), 0);
            });
            
            REQUIRE(result == std::vector<usize>{0, 2, 5, 6});
        }
        
        SECTION("Empty") {
            std::vector<i64> data = {};
            auto result = crnn::util::locations_of_change(data.begin(), data.end());
            REQUIRE(result == std::vector<usize>{});
        }
    }
    
    SECTION("Block locating in the generated boundary deltas") {
        // Empty stuff doesn't work/is UB.
        std::vector<i64> data = {0, 0, 0, 1, 1, 2, 3, 3, 2, 2};
        auto change_deltas = crnn::util::locations_of_change(data.begin(), data.end());
        using iter_t = decltype(data)::iterator;
        
        auto nth_block = [&](usize n) {
            return crnn::util::get_nth_data_block(data.begin(), data.end(), change_deltas, n);
        };
        
        auto idx_iter_pair = [&](usize s, usize e) {
            return std::pair {
                s == data.size() ? data.end() : data.begin() + s,
                e == data.size() ? data.end() : data.begin() + e
            };
        };
        
        std::array<std::pair<iter_t, iter_t>, 5> blocks = {
            nth_block(0),
            nth_block(1),
            nth_block(2),
            nth_block(3),
            nth_block(4)
        };
        
        REQUIRE(blocks == std::array<std::pair<iter_t, iter_t>, 5>{
            idx_iter_pair(0, 3),
            idx_iter_pair(3, 5),
            idx_iter_pair(5, 6),
            idx_iter_pair(6, 8),
            idx_iter_pair(8, 10)
        });
    }
}


